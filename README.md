<h1>Hotel Reservation System
<img src="https://linuxbsdos.com/wp-content/uploads/2015/11/oracle-java-700x410.png" height="3%"/> 
</h1>

### REST application to manage hotel reservation process.
This application uses Java for beck-end and JavaScript for front-end (you can run this application from index.html). 
It uses Spring Security to differentiate it users (Employees and Guests) back-end access and front-end views/pages.
Database H2 is used to collect data.

#### Technologies
- Java 17
- Spring Boot
- Spring Security
- JPA
- H2
- Lombok
- Mapstruct
- JWT
- JavaScript
- CSS

#### External APIs
- [Free Weather IP](http://api.weatherapi.com)

Setup
-----
- Clone repository.
       git clone https://gitlab.com/InnaM/hotel-reservation-system.git
- Install the latest version of [Java 17](https://java.com), [Maven](https://maven.apache.org/download.cgi).
- You may need to set your `JAVA_HOME`.

#### bash
$ mvn compile install
$ mvn spring-boot:run

## or
$ java -jar target/ReservationSystem-0.0.1-SNAPSHOT.jar