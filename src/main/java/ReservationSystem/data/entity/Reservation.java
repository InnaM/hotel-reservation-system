package ReservationSystem.data.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import java.time.LocalDateTime;

@Entity
@Builder
@Table(name = "RESERVATION")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Reservation {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "RESERVATION_ID")
    private String reservationId;

    @ManyToOne
    @NotNull(groups = {AddRoomGroup.class}, message = "The room can not be null")
    private Room room;

    @ManyToOne
    @NotNull(groups = {AddRoomGroup.class}, message = "Guest can not be null")
    private Guest guest;

    @Column(columnDefinition = "TIMESTAMP",
            name = "START_DATE")
    @NotNull
    private LocalDateTime startDate;

    @Column(columnDefinition = "TIMESTAMP",
            name = "END_DATE")
    @NotNull
    private LocalDateTime endDate;


}
