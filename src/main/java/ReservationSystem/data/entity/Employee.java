package ReservationSystem.data.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
public class Employee {
    @Id
    @Column(name = "USERNAME")
    @NotNull
    @NotBlank
    @Size(max = 50)
    private String username;

    @Column(name = "FULL_NAME")
    @NotNull
    @NotBlank
    @Size(max = 50)
    private String fullName;

    @Column(name = "PASSWORD")
    @NotNull
    @NotBlank
    private String password;
    private String position;

}
