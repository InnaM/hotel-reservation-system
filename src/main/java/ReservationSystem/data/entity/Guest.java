package ReservationSystem.data.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import java.util.LinkedList;
import java.util.List;


@Entity
@Table(name = "GUEST")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class Guest {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @Column(name = "LOGIN")
    private String login;

    @Column(name = "FIRST_NAME")
    @NotNull(groups = {AddRoomGroup.class}, message = "First Name can not be null")
    private String firstName;

    @Column(name = "LAST_NAME")
    @NotNull(groups = {AddRoomGroup.class}, message = "Last Name Name can not be null")
    private String lastName;

    @Column(name = "PASSWORD")
    private String password;
    @Column(name = "EMAIL")
    private String email;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "PHONE_NUMBER")
    @NotNull(message = "Phone can't be null")
    private String phoneNumber;

    @OneToMany(mappedBy = "guest", cascade = CascadeType.ALL)
    private List<Reservation> reservations = new LinkedList<>();


}
