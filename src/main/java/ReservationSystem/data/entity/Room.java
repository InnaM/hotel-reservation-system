package ReservationSystem.data.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "ROOM")
public class Room {

    public Room() {
    }

    public Room(String roomNumber, BedInfo bedInfo, List<Reservation> reservations) {
        this.roomNumber = roomNumber;
        this.bedInfo = bedInfo;
        this.reservations = reservations;
    }
    @Id
    @Column (name = "ROOM_NUMBER")
    @NotNull(groups = {AddRoomGroup.class}, message = "The room number can not be null")
    private String roomNumber;

    @Column (name = "BED_INFO")
    @Enumerated(EnumType.STRING)
    @NotNull(message = "Bed info can't be null")
    private BedInfo bedInfo;

    @OneToMany(mappedBy = "room",
            cascade = CascadeType.ALL)
    private List<Reservation> reservations = new ArrayList<>();

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public BedInfo getBedInfo() {
        return bedInfo;
    }

    public void setBedInfo(BedInfo bedInfo) {
        this.bedInfo = bedInfo;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    @Override
    public String toString() {
        return "Room{" +
                ", roomNumber='" + roomNumber + '\'' +
                ", bedInfo='" + bedInfo + '\'' +
                ", reservations=" + reservations +
                '}';
    }
}
