package ReservationSystem.data.dto;

import ReservationSystem.data.entity.BedInfo;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class RoomDTO {
    private String roomNumber;
    private BedInfo bedInfo;

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public BedInfo getBedInfo() {
        return bedInfo;
    }

    public void setBedInfo(BedInfo bedInfo) {
        this.bedInfo = bedInfo;
    }

}
