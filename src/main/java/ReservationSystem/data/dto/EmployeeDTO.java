package ReservationSystem.data.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class EmployeeDTO {
    private String username;
    private String fullName;
    private String password;
    private String position;

}
