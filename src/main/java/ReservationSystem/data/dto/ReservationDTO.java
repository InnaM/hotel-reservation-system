package ReservationSystem.data.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.*;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class ReservationDTO {

    private String reservationId;

    private String roomNumber;

    @JsonIgnore
    private String guestLogin;

    private String guestFirstName;

    private String guestLastName;

    private String guestEmail;

    private String guestAddress;

    private String guestPhoneNumber;

    @JsonFormat(pattern = "dd.MM.yyyy HH:mm")
    private LocalDateTime startDate;

    @JsonFormat(pattern = "dd.MM.yyyy HH:mm")
    private LocalDateTime endDate;
}
