package ReservationSystem.util;

import ReservationSystem.data.dto.EmployeeDTO;
import ReservationSystem.data.dto.GuestDTO;
import ReservationSystem.data.dto.ReservationDTO;
import ReservationSystem.data.dto.RoomDTO;
import ReservationSystem.data.entity.*;
import ReservationSystem.repository.*;
import ReservationSystem.service.*;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;

import org.springframework.stereotype.Component;
import java.time.LocalDateTime;
import java.util.Collections;

@Component
@RequiredArgsConstructor
public class DemoCommandLineRunner implements CommandLineRunner {

    private final RoomService roomService;
    private final GuestService guestService;
    private final ReservationService reservationService;
    private final EmployeeService employeeservice;
    private final RoleRepository roleRepository;
    private final RoleService roleService;


    @Override
    public void run(String... args) throws Exception {
        /*RoomDTO room1 = new RoomDTO("101", BedInfo.TWIN_SINGLES);
        roomService.addRoom(room1);
        RoomDTO room2 = new RoomDTO("102", BedInfo.SINGLE);
        roomService.addRoom(room2);
        RoomDTO room3 = new RoomDTO("103", BedInfo.SUPER_KING);
        roomService.addRoom(room3);
        RoomDTO room4 = new RoomDTO("104", BedInfo.TWIN_SINGLES);
        roomService.addRoom(room4);
        RoomDTO room5 = new RoomDTO("105", BedInfo.SINGLE);
        roomService.addRoom(room5);
        RoomDTO room6 = new RoomDTO("106", BedInfo.SUPER_KING);
        roomService.addRoom(room6);
        RoomDTO room7 = new RoomDTO("107", BedInfo.TWIN_SINGLES);
        roomService.addRoom(room7);
        RoomDTO room8 = new RoomDTO("108", BedInfo.SINGLE);
        roomService.addRoom(room8);
        RoomDTO room9 = new RoomDTO("109", BedInfo.TWIN_SINGLES);
        roomService.addRoom(room9);
        RoomDTO room10 = new RoomDTO("110", BedInfo.TWIN_SINGLES);
        roomService.addRoom(room10);
        RoomDTO room11 = new RoomDTO("111", BedInfo.SINGLE);
        roomService.addRoom(room11);
        RoomDTO room12 = new RoomDTO("112", BedInfo.TWIN_SINGLES);
        roomService.addRoom(room12);
        RoomDTO room13 = new RoomDTO("113", BedInfo.SINGLE);
        roomService.addRoom(room13);
        RoomDTO room14 = new RoomDTO("114", BedInfo.TWIN_SINGLES);
        roomService.addRoom(room14);
        RoomDTO room15 = new RoomDTO("115", BedInfo.TWIN_SINGLES);
        roomService.addRoom(room15);
        RoomDTO room16 = new RoomDTO("116", BedInfo.SINGLE);
        roomService.addRoom(room16);
        RoomDTO room17 = new RoomDTO("117", BedInfo.TWIN_SINGLES);
        roomService.addRoom(room17);
        RoomDTO room18 = new RoomDTO("18", BedInfo.TWIN_SINGLES);
        roomService.addRoom(room18);
        RoomDTO room19 = new RoomDTO("119", BedInfo.SINGLE);
        roomService.addRoom(room19);
        RoomDTO room20 = new RoomDTO("120", BedInfo.TWIN_SINGLES);
        roomService.addRoom(room20);
        RoomDTO room21 = new RoomDTO("121", BedInfo.TWIN_SINGLES);
        roomService.addRoom(room21);
        RoomDTO room22 = new RoomDTO("122", BedInfo.SINGLE);
        roomService.addRoom(room22);

        Role adminRole = new Role(null, EnumRole.ADMIN, Collections.emptySet());
        Role userRole = new Role(null, EnumRole.USER, Collections.emptySet());
        Role employeeRole = new Role(null, EnumRole.EMPLOYEE, Collections.emptySet());
        roleRepository.save(adminRole);
        roleRepository.save(userRole);
        roleRepository.save(employeeRole);

        EmployeeDTO manager = new EmployeeDTO("admin", "Jan Kowalski", "Admin1", "Manager");
        employeeservice.addEmployee(manager);

        EmployeeDTO receptionist = new EmployeeDTO("nowak", "Ewa Nowak",
                "Nowak1", "Receptionist");
        employeeservice.addEmployee(receptionist);

        GuestDTO guest1 = new GuestDTO(
                "guest1", "Adam", "Guest", "Guest1",
                "email1@com", "Address1, Address1, Address1, Address1 ", "23-984534623");
        guestService.addGuestByUser(guest1);

        GuestDTO guest2 = new GuestDTO(
                "guest2", "Erick", "Guest2", null,
                "email2@com", "Address2, Address2, Address2, Address2 ", "48-243567901");

        GuestDTO guest3 = new GuestDTO(
                "guest3", "Mary", "Guest", null,
                "email3@com", "Address3, Address3, Address3, Address3 ", "370-12557696794");

        ReservationDTO reservation1 = reservationService.addReservationByUser(new AvailableRoomRequest(BedInfo.TWIN_SINGLES,guest1.getLogin(),
                LocalDateTime.of(2025, 8, 20, 14, 00),
                LocalDateTime.of(2025, 8, 22, 12, 00)));

        ReservationDTO reservation2 = new ReservationDTO(
                null, room2.getRoomNumber(), null, guest1.getFirstName(),
                guest1.getLastName(), guest1.getEmail(), guest1.getAddress(), guest1.getPhoneNumber(),
                LocalDateTime.of(2025, 8, 28, 14, 00),
                LocalDateTime.of(2025, 8, 30, 12, 00));
        reservationService.addReservationByAdmin(reservation2);

        ReservationDTO reservation3 = new ReservationDTO(
                null, room3.getRoomNumber(), guest2.getLogin(), guest2.getFirstName(),
                guest2.getLastName(), guest2.getEmail(), guest2.getAddress(), guest2.getPhoneNumber(),
                LocalDateTime.of(2025, 8, 21, 14, 00),
                LocalDateTime.of(2025, 8, 25, 12, 00));
        reservationService.addReservationByAdmin(reservation3);*/
    }
}
