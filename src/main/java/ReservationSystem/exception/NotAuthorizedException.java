package ReservationSystem.exception;

public class NotAuthorizedException extends  RuntimeException{
    public NotAuthorizedException(){
        super("Incorrect login or password");
    }
}
