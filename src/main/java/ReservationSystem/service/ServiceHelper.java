package ReservationSystem.service;

import java.time.LocalDateTime;

public class ServiceHelper {

    public static boolean passwordValidation(String password) {
        String regex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{6,}$";
        if (password == null) {
            return false;
        }
        return password.matches(regex);
    }

    public static boolean usernameValidation(String username) {
        if (username == null) {
            return false;
        }
        return username.length() >= 3 && username.length() <= 10;
    }

    public static boolean validateDate(LocalDateTime date, int hours) {
        if (date == null) {
            return false;
        }
        LocalDateTime today = LocalDateTime.now();
        today = today.withHour(hours).withSecond(00);
        return date.isBefore(today);
    }

}
