package ReservationSystem.service;

import ReservationSystem.data.dto.RoomDTO;
import ReservationSystem.data.entity.BedInfo;
import ReservationSystem.data.entity.Reservation;
import ReservationSystem.data.entity.Room;
import ReservationSystem.mapper.RoomMapper;
import ReservationSystem.repository.ReservationRepository;
import ReservationSystem.repository.RoomRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Transactional
@Service
public class RoomService {

    private final Logger logger = LogManager.getLogger(RoomService.class);
    private final RoomRepository roomRepository;

    private final ReservationRepository reservationRepository;
    private RoomMapper mapper;

    public RoomService(RoomRepository roomRepository, ReservationRepository reservationRepository, RoomMapper mapper) {
        this.roomRepository = roomRepository;
        this.reservationRepository = reservationRepository;
        this.mapper = mapper;
    }

    public RoomDTO addRoom(RoomDTO roomDTO) {
        Room room = mapper.fromDto(roomDTO);
        roomRepository.findByRoomNumber(room.getRoomNumber())
                .ifPresent(r -> {
                    throw new IllegalArgumentException("Room with this number already exist");
                });
        return mapper.toDto(roomRepository.save(room));
    }

    public RoomDTO deleteByNumber(String number) {
        Room room = roomRepository.findByRoomNumber(number)
                .orElseThrow(() ->
                        new NoSuchElementException("Room with this number don't exist"));
        roomRepository.deleteByRoomNumber(room.getRoomNumber());
        return mapper.toDto(room);
    }


    public RoomDTO updateRoomInfo(RoomDTO roomDTO) {
        Room room = mapper.fromDto(roomDTO);
        Room roomToUpdate = roomRepository.findByRoomNumber(room.getRoomNumber()).orElseThrow(() ->
                new NoSuchElementException("There is no room with Number: " + roomDTO.getRoomNumber()));

        if (room.getBedInfo() != null) {
            roomToUpdate.setBedInfo(room.getBedInfo());
        }
        return mapper.toDto(roomRepository.save(roomToUpdate));
    }


    public List<RoomDTO> getAll(String direction) {
        Sort sort;
        if ("asc".equals(direction)) {
            sort = Sort.by(Sort.Order.asc("roomNumber"));
        } else if ("desc".equals(direction)) {
            sort = Sort.by(Sort.Order.desc("roomNumber"));
        } else {
            sort = Sort.unsorted();
        }
        return roomRepository.findAll(sort).stream()
                .map(r -> mapper.toDto(r))
                .collect(Collectors.toList());
    }

    public RoomDTO getByRoomNumber(String number) {
        return mapper.toDto(roomRepository.findByRoomNumber(number).orElseThrow(() ->
                new NoSuchElementException("There is no room with number: " + number)));
    }

    public List<RoomDTO> getByBedInfo(String bedInfo) {
        return roomRepository.findByBedInfo(BedInfo.valueOf(bedInfo)).stream()
                .map(r -> mapper.toDto(r))
                .collect(Collectors.toList());
    }

    public List<RoomDTO> getAvailableRooms(AvailableRoomRequest availableRoomRequest) {
        if (ServiceHelper.validateDate(availableRoomRequest.getStartDate(), 12)) {
            throw new IllegalArgumentException(
                    "Wrong Start Date.");
        }

        if (ServiceHelper.validateDate(availableRoomRequest.getEndDate(), 14)) {
            throw new IllegalArgumentException(
                    "Wrong End Date.");
        }

        if (availableRoomRequest.getEndDate().isBefore(availableRoomRequest.getEndDate())) {
            throw new IllegalArgumentException(
                    "End Date couldn't be earlier than Start Date.");
        }
        BedInfo bedInfo = availableRoomRequest.getBedInfo();
        List<Room> roomsByBedInfo = new ArrayList<>();
        List<Reservation>  reservations = new ArrayList<>();

        if(bedInfo!=null && !bedInfo.equals("")) {
            roomsByBedInfo = roomRepository.findByBedInfo(availableRoomRequest.getBedInfo());
            reservations = reservationRepository
                    .findByRoom_BedInfoAndStartDateLessThanEqualAndEndDateGreaterThanEqual(availableRoomRequest.getBedInfo(),
                            availableRoomRequest.getEndDate(),
                            availableRoomRequest.getStartDate());
            logger.debug(reservations.size());

        } else {
            roomsByBedInfo = roomRepository.findAll();
            reservations = reservationRepository
                    .findByStartDateLessThanEqualAndEndDateGreaterThanEqual(availableRoomRequest.getEndDate(),
                            availableRoomRequest.getStartDate());
            logger.debug(reservations.size());
        }

        if (!reservations.isEmpty()) {
            Set<Room> bookedRooms = reservations.stream()
                    .map(res -> res.getRoom())
                    .collect(Collectors.toSet());
            logger.debug("Rooms size booked at the same period: " + roomsByBedInfo.size());

            for(Room room: bookedRooms){
                roomsByBedInfo.remove(room);
            }
            logger.debug("Available rooms size: " + roomsByBedInfo.size());

            if (roomsByBedInfo.isEmpty()) {
                return Collections.emptyList();
            } else {
                return roomsByBedInfo.stream()
                        .map(room -> mapper.toDto(room))
                        .collect(Collectors.toList());
            }
        } else {
            return roomsByBedInfo.stream()
                    .map(room -> mapper.toDto(room))
                    .collect(Collectors.toList());
        }
    }

    public RoomDTO getAvailableRoom(AvailableRoomRequest availableRoomRequest) {
        return getAvailableRooms(availableRoomRequest).stream()
                .findFirst()
                .orElseThrow(() ->
                        new NoSuchElementException("There are no available rooms in such period"));
    }
}

