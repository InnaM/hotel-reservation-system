package ReservationSystem.service;

import ReservationSystem.data.dto.ReservationDTO;
import ReservationSystem.data.dto.RoomDTO;
import ReservationSystem.data.entity.Guest;
import ReservationSystem.data.entity.Reservation;
import ReservationSystem.data.entity.Room;
import ReservationSystem.mapper.ReservationMapper;
import ReservationSystem.mapper.RoomMapper;
import ReservationSystem.repository.GuestRepository;
import ReservationSystem.repository.ReservationRepository;
import ReservationSystem.repository.RoomRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Transactional
@Service
public class ReservationService {

    private final ReservationRepository reservationRepository;
    private final RoomRepository roomRepository;

    private final RoomService roomService;
    private final GuestRepository guestRepository;
    private ReservationMapper mapper;

    private RoomMapper roomMapper;

    private final Logger logger = LogManager.getLogger(ReservationService.class);

    @Autowired
    public ReservationService(ReservationRepository reservationRepository, RoomRepository roomRepository, GuestRepository guestRepository, RoomService roomService, ReservationMapper mapper, RoomMapper roomMapper) {
        this.reservationRepository = reservationRepository;
        this.roomRepository = roomRepository;
        this.roomService = roomService;
        this.guestRepository = guestRepository;
        this.mapper = mapper;
        this.roomMapper = roomMapper;
    }

    public ReservationDTO addReservationByAdmin(ReservationDTO reservationDTO) {
        if(reservationDTO.getGuestFirstName()==null || reservationDTO.getGuestFirstName().equals("")) {
            throw new IllegalArgumentException(
                "First Name couldn't be empty.");
        }

        if(reservationDTO.getGuestLastName()==null || reservationDTO.getGuestLastName().equals("")) {
            throw new IllegalArgumentException(
                    "Last Name couldn't be empty.");
        }

        if (ServiceHelper.validateDate(reservationDTO.getStartDate(), 12)) {
            throw new IllegalArgumentException(
                    "Start Date has already passed.");
        }

        if (ServiceHelper.validateDate(reservationDTO.getEndDate(), 14)) {
            throw new IllegalArgumentException(
                    "End Date has already passed.");
        }

        if (reservationDTO.getEndDate().isBefore(reservationDTO.getEndDate())) {
            throw new IllegalArgumentException(
                    "End Date couldn't be earlier than Start Date.");
        }

        Reservation reservation = mapper.fromDto(reservationDTO);
        reservationRepository.findByRoom_RoomNumberAndStartDateLessThanAndEndDateGreaterThanEqual(reservation.getRoom().getRoomNumber(), reservation.getEndDate(), reservation.getStartDate())
                .ifPresent(r -> {
                    throw new IllegalArgumentException(
                            "There is another Reservation for this Room during this period.\nStart: " + r.getStartDate() + "\nEnd: " + r.getEndDate());
                });
        Room room = roomRepository.findByRoomNumber(reservation.getRoom().getRoomNumber()).orElseThrow(() ->
                new NoSuchElementException("There is no such Room in the hotel"));


        Guest guest = guestRepository.findByFirstNameAndLastName(reservationDTO.getGuestFirstName(), reservationDTO.getGuestLastName()).orElse(null);
        if (guest == null) {
            guest = new Guest(null, null, reservationDTO.getGuestFirstName(), reservationDTO.getGuestLastName(),
                    null, reservationDTO.getGuestEmail(), reservationDTO.getGuestAddress(), reservationDTO.getGuestPhoneNumber(), Collections.emptyList());
            guest = guestRepository.save(guest);
        }
        reservation.setRoom(room);
        reservation.setGuest(guest);
        return mapper.toDto(reservationRepository.save(reservation));
    }



    public ReservationDTO deleteById(String id) {
        Reservation reservation = reservationRepository.findById(id).orElseThrow(
                () -> new NoSuchElementException("This reservation doesn't exist"));
        reservationRepository.deleteById(id);
        return mapper.toDto(reservation);
    }

    public ReservationDTO updateReservation(ReservationDTO reservationDTO) {
        if (ServiceHelper.validateDate(reservationDTO.getStartDate(), 12)) {
            throw new IllegalArgumentException(
                    "Wrong Start Date.");
        }

        if (ServiceHelper.validateDate(reservationDTO.getEndDate(), 14)) {
            throw new IllegalArgumentException(
                    "Wrong End Date.");
        }

        if (reservationDTO.getEndDate().isBefore(reservationDTO.getEndDate())) {
            throw new IllegalArgumentException(
                    "End Date couldn't be earlier than Start Date.");
        }
        boolean shouldVerify = false;
        Reservation reservation = mapper.fromDto(reservationDTO);
        Reservation reservationToUpdate = reservationRepository.findById(reservation.getReservationId()).orElseThrow(
                () -> new NoSuchElementException("This reservation doesn't exist"));

        if (reservation.getRoom() != null) {
            Room room = roomRepository.findByRoomNumber(reservation.getRoom().getRoomNumber()).orElseThrow(() -> {
                throw new NoSuchElementException("Such room doesn't exist in the hotel");
            });
            reservationToUpdate.setRoom(room);
            shouldVerify = true;
        }
        if (reservation.getGuest() != null) {
            Guest guest = guestRepository.findByFirstNameAndLastName(reservation.getGuest().getFirstName(), reservation.getGuest().getLastName()).orElse(null);
            if (guest == null) {
                guest = new Guest(null, null, reservationDTO.getGuestFirstName(), reservationDTO.getGuestLastName(),
                        null, reservationDTO.getGuestEmail(), reservationDTO.getGuestAddress(), reservationDTO.getGuestPhoneNumber(), Collections.emptyList());
                guest = guestRepository.save(guest);
            }
            reservationToUpdate.setGuest(guest);
            shouldVerify = true;
        }
        if (reservation.getStartDate() != null) {
            reservationToUpdate.setStartDate(reservation.getStartDate());
            shouldVerify = true;
        }
        if (reservation.getEndDate() != null) {
            reservationToUpdate.setEndDate(reservation.getEndDate());
            shouldVerify = true;
        }
        if (shouldVerify) {
           reservationRepository.findByRoom_RoomNumberAndStartDateLessThanAndEndDateGreaterThanEqual(
                    reservationToUpdate.getRoom().getRoomNumber(),
                    reservationToUpdate.getEndDate(),
                    reservationToUpdate.getStartDate())
                    .filter(r -> !r.equals(reservationToUpdate))
                    .ifPresent(r -> {
                        throw new IllegalArgumentException(
                                "There is another Reservation for this Room during this period.\nStart: " + r.getStartDate() + "\nEnd: " + r.getEndDate());
                    });
        }
        return mapper.toDto(reservationRepository.save(reservationToUpdate));
    }

    public ReservationDTO getById(String id) {
        return mapper.toDto(reservationRepository.findById(id).orElseThrow(
                () -> new NoSuchElementException("This reservation doesn't exist")));
    }


    public List<ReservationDTO> getByUsername(String username) {
        List<ReservationDTO> reservations = reservationRepository.findByGuest_Login(username).stream()
                .sorted((r1, r2) -> {
                    return r1.getStartDate().compareTo(r2.getStartDate());
                })
                .map(r -> mapper.toDto(r))
                .collect(Collectors.toList());
            return reservations;
    }

    public List<ReservationDTO> getByFullName(String firstName, String lastName) {
        return reservationRepository.findByGuest_FirstNameAndGuest_LastName(firstName,lastName).stream()
                .sorted((r1, r2) -> {
                    return r1.getStartDate().compareTo(r2.getStartDate());
                })
                .map(r -> mapper.toDto(r))
                .collect(Collectors.toList());
    }

    public List<ReservationDTO> getAll() {
        return reservationRepository.findAll().stream()
                .sorted((r1, r2) -> {
                    return r1.getStartDate().compareTo(r2.getStartDate());
                })
                .map(r -> mapper.toDto(r))
                .collect(Collectors.toList());
    }

    public List<ReservationDTO> getAllFiltered(String reservationId, String roomNumber, String firstName, String lastName) {

        List<Reservation> reservations = reservationRepository.findAll();
        if (reservationId != null) {
            reservations = reservations.stream()
                    .filter(r -> r.getReservationId().equals(reservationId))
                    .collect(Collectors.toList());
        }
        if (roomNumber != null) {
            reservations = reservations.stream()
                    .filter(r -> r.getRoom().getRoomNumber().equals(roomNumber))
                    .collect(Collectors.toList());
        }
        if (firstName != null) {
            reservations = reservations.stream()
                    .filter(r -> r.getGuest().getFirstName().equals(firstName))
                    .collect(Collectors.toList());
        }

        if (lastName != null) {
            reservations = reservations.stream()
                    .filter(r -> r.getGuest().getLastName().equals(lastName))
                    .collect(Collectors.toList());
        }

        return reservations.stream().sorted((r1, r2) -> {
                    return r1.getStartDate().compareTo(r2.getStartDate());
                })
                .map(r -> mapper.toDto(r))
                .collect(Collectors.toList());
    }

    public ReservationDTO addReservationByUser(AvailableRoomRequest availableRoomRequest) {
        if (ServiceHelper.validateDate(availableRoomRequest.getStartDate(), 12)) {
            throw new IllegalArgumentException(
                    "Wrong Start Date.");
        }

        if (ServiceHelper.validateDate(availableRoomRequest.getEndDate(), 14)) {
            throw new IllegalArgumentException(
                    "Wrong End Date.");
        }

        if (availableRoomRequest.getEndDate().isBefore(availableRoomRequest.getEndDate())) {
            throw new IllegalArgumentException(
                    "End Date couldn't be earlier than Start Date.");
        }

        RoomDTO availableRoomDTO = roomService.getAvailableRoom(availableRoomRequest);
        Room room = roomMapper.fromDto(availableRoomDTO);
        Guest guest = guestRepository.findByLogin(availableRoomRequest.getUsername()).orElseThrow(() -> {
            throw new NoSuchElementException("This guest doesn't exist in the hotel base");
        });
        Reservation newReservation = new Reservation(null, room, guest, availableRoomRequest.getStartDate(), availableRoomRequest.getEndDate());
        reservationRepository.findByRoom_RoomNumberAndStartDateLessThanAndEndDateGreaterThanEqual(newReservation.getRoom().getRoomNumber(), newReservation.getEndDate(), newReservation.getStartDate())
                .ifPresent(r -> {
                    throw new IllegalArgumentException(
                            "There is another Reservation for this Room during this period.\nStart: " + r.getStartDate() + "\nEnd: " + r.getEndDate());
                });

        return mapper.toDto(reservationRepository.save(newReservation));
    }





}
