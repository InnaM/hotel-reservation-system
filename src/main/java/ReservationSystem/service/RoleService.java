package ReservationSystem.service;

import ReservationSystem.data.entity.EnumRole;
import ReservationSystem.data.entity.Role;
import ReservationSystem.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@RequiredArgsConstructor
@Slf4j
@Transactional
public class RoleService {
    private final RoleRepository roleRepository;

    public Role saveRole(Role role) {
        log.info("Saving new Role {}", role.getName());

        roleRepository.findByName(role.getName()).ifPresent(r -> {
            throw new IllegalArgumentException("This Role already exist");
        });
        return roleRepository.save(role);
    }

    public Role deleteRoleByName(String roleName) {
        Role role = getRoleFromString(roleName);
        log.info("Deleting Role {}", roleName);
        roleRepository.delete(role);
        return role;
    }

    public List<Role> allRoles() {
        log.info("Fetching all Roles ");
        return roleRepository.findAll();
    }

    public Role getRoleByName(String name) {
        log.info("Fetching Role by name {}", name);
        return getRoleFromString(name);
    }

    public Role getRoleFromString(String roleName) {
        Optional<EnumRole> roleEnum = Arrays.stream(EnumRole.values())
                .filter(v -> v.name().contains(roleName.toUpperCase()))
                .findAny().or(() -> {
                    throw new NoSuchElementException("Incorrect Role name");
                });
        return roleRepository.findByName(roleEnum.get()).orElseThrow(() ->
                new NoSuchElementException("There is no such role"));
    }

    public boolean addUserToRole(Role role, String fullName) {
        return role.getUsers().add(fullName);
    }
}
