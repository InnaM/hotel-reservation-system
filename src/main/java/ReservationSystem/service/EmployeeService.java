package ReservationSystem.service;

import ReservationSystem.data.dto.EmployeeDTO;
import ReservationSystem.data.entity.*;
import ReservationSystem.mapper.EmployeeMapper;
import ReservationSystem.repository.RoleRepository;
import ReservationSystem.repository.EmployeeRepository;
import ReservationSystem.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class EmployeeService {
    private final EmployeeRepository employeeRepository;
    private final RoleRepository roleRepository;
    private final EmployeeMapper mapper;
    private final UserRepository userRepository;
    private final RoleService roleService;

    public EmployeeDTO addEmployee(EmployeeDTO employeeDto) {
        log.info("Saving new employee {} to the Database", employeeDto.getUsername());
        EnumRole roleString = fetchRoleFromPosition(employeeDto.getPosition());
        Role role = roleRepository.findByName(roleString).orElseThrow(() -> new NoSuchElementException("There is no such role"));
        Employee employee = mapper.fromDto(employeeDto);
        employeeRepository.findByUsername(employee.getUsername()).ifPresent(e -> {
            throw new IllegalArgumentException("This Employee already exist: " + employee.getUsername());
        });
        boolean isValidUsername = ServiceHelper.usernameValidation(employee.getUsername());
        if (!isValidUsername) {
            throw new IllegalArgumentException("Username should contains at least 3 at last 10 characters.");
        }

        boolean isValidPassword = ServiceHelper.passwordValidation(employee.getPassword());
        if (!isValidPassword) {
            throw new IllegalArgumentException("Password should contains at least 6 characters, including at least one lowercase letter, one uppercase letter and number.");
        }
        userRepository.save(new User(employee.getUsername(),
                new BCryptPasswordEncoder().encode(employee.getPassword()), role.getName()));
        roleService.addUserToRole(role,employee.getFullName());
       // role.getUsers().add(employee.getFullName());
        return mapper.toDto(employeeRepository.save(employee));
    }


    public EmployeeDTO getEmployeeByUserName(String username) {
        log.info("Fetching  Employee {} from the Database", username);
        return mapper.toDto(employeeRepository.findByUsername(username).orElseThrow(() -> new NoSuchElementException("There is no user with such username")));
    }

    public List<EmployeeDTO> getAllEmployees() {
        log.info("Fetching  all employees");
        return employeeRepository.findAll().stream().map(e -> mapper.toDto(e)).collect(Collectors.toList());
    }

    private EnumRole fetchRoleFromPosition(String position) {
        EnumRole role = null;
        role = switch (position) {
            case "Director", "Manager" ->  EnumRole.ADMIN;
            case "Receptionist" ->  EnumRole.EMPLOYEE;
            default -> throw new IllegalStateException("Unexpected value: " + role);
        };
        return role;
    }
    public EmployeeDTO deleteByFullName(String fullName) {
        Employee employee = employeeRepository.findByFullName(fullName).orElseThrow(() ->
                new NoSuchElementException("Employee with full name: " + fullName + " doesn't exist."));
        employeeRepository.deleteByFullName(employee.getFullName());
        userRepository.deleteById(employee.getUsername());
        return mapper.toDto(employee);
    }

    public EmployeeDTO deleteByUsername(String username) {
        Employee employee = employeeRepository.findByUsername(username).orElseThrow(() ->
                new NoSuchElementException("Employee with username: " + username + " doesn't exist."));
        employeeRepository.deleteByUsername(employee.getUsername());
        userRepository.deleteById(employee.getUsername());
        return mapper.toDto(employee);
    }

    public EmployeeDTO updateEmployee(EmployeeDTO employeeDTO) {
        Employee employee = mapper.fromDto(employeeDTO);
        Employee employeeToUpdate = employeeRepository.findByUsername(employee.getUsername()).orElseThrow(() ->
                new NoSuchElementException("Employee with id: " + employee.getUsername() + " doesn't exist."));
        User user = userRepository.findByUsername(employeeToUpdate.getUsername()).orElseThrow(() ->
                new NoSuchElementException("User with login: " + employee.getUsername() + " doesn't exist."));
        if (employee.getFullName() != null) {
            employeeToUpdate.setFullName(employee.getFullName());
        }

        if (employee.getPassword() != null) {
            boolean isValidPassword = ServiceHelper.passwordValidation(employee.getPassword());
            if (!isValidPassword) {
                throw new IllegalArgumentException("Password should contains at least 6 characters, including at least one lowercase letter, one uppercase letter and number.");
            }
            employeeToUpdate.setPassword(employee.getPassword());
        }
        user.setUsername(employee.getUsername());
        user.setPassword(new BCryptPasswordEncoder().encode(employee.getPassword()));
        return mapper.toDto(employeeRepository.save(employeeToUpdate));
    }
}
