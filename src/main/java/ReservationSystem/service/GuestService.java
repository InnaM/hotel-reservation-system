package ReservationSystem.service;

import ReservationSystem.data.dto.GuestDTO;

import ReservationSystem.data.entity.EnumRole;
import ReservationSystem.data.entity.Role;
import ReservationSystem.data.entity.Guest;
import ReservationSystem.mapper.GuestMapper;
import ReservationSystem.repository.RoleRepository;
import ReservationSystem.repository.GuestRepository;
import ReservationSystem.data.entity.User;
import ReservationSystem.repository.UserRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class GuestService {
    private final GuestMapper guestMapper;
    private final RoleRepository roleRepository;

    private final UserRepository userRepository;

    private final GuestRepository guestRepository;

    private final RoleService roleService;

    public GuestDTO addGuestByUser(GuestDTO guestDto) {
        Role userRole = roleRepository.findByName(EnumRole.USER).orElseThrow(() -> new NoSuchElementException("There is no Role for Guest"));

        if (guestDto.getLogin() != null && !guestDto.getLogin().isEmpty()) {
            userRepository.findByUsername(guestDto.getLogin()).ifPresent(g -> {
                throw new IllegalArgumentException("User with this login already exist, please change username");
            });
        }
        boolean isValidUsername = ServiceHelper.usernameValidation(guestDto.getLogin());
        if (!isValidUsername) {
            throw new IllegalArgumentException("Username should contains at last 10 characters.");
        }

        boolean isValidPassword = ServiceHelper.passwordValidation(guestDto.getPassword());
        if (!isValidPassword) {
            throw new IllegalArgumentException("Password should contains at least 6 characters, including at least one lowercase letter, one uppercase letter and number.");
        }

        Guest existGuest = guestRepository.findByFirstNameAndLastName(guestDto.getFirstName(), guestDto.getLastName()).orElse(null);
        Guest guest = null;
        if (existGuest != null) {
            return updateGuest(guestDto);
        } else {
            guest = guestMapper.fromDto(guestDto);
            guest.setReservations(Collections.emptyList());
            userRepository.save(new User(guest.getLogin(),
                    new BCryptPasswordEncoder().encode(guest.getPassword()),
                    userRole.getName()));
            roleService.addUserToRole(userRole,guest.getFirstName() + " " + guest.getLastName());
        }
            return guestMapper.toDto(guestRepository.save(guest));
    }

    public GuestDTO deleteByLogin(String login) {
        Guest guest = guestRepository.findByLogin(login).orElseThrow(() ->
                new NoSuchElementException("Guest with username: " + login + " doesn't exist."));
        guestRepository.deleteByLogin(guest.getLogin());
        userRepository.deleteById(guest.getLogin());
        return guestMapper.toDto(guest);
    }


    public GuestDTO updateGuest(GuestDTO guestDTO) {
        Guest guest = guestMapper.fromDto(guestDTO);
        Guest guestToUpdate = guestRepository.findByFirstNameAndLastName(guest.getFirstName(), guest.getLastName()).orElseThrow(() ->
                new NoSuchElementException("Guest with id: " + guest.getLogin() + " doesn't exist."));


        if (guest.getLogin() != null && !guest.getLogin().equals(guestToUpdate.getLogin())) {
            guestToUpdate.setLogin(guest.getLogin());
        }

        if (guest.getFirstName() != null && !guest.getFirstName().equals(guestToUpdate.getFirstName())) {
            guestToUpdate.setFirstName(guest.getFirstName());
        }
        if (guest.getLastName() != null && !guest.getLastName().equals(guestToUpdate.getLastName())) {
            guestToUpdate.setLastName(guest.getLastName());
        }
        if (guest.getPassword() != null && !guest.getPassword().equals(guestToUpdate.getPassword())) {
            boolean isValidPassword = ServiceHelper.passwordValidation(guest.getPassword());
            if (!isValidPassword) {
                throw new IllegalArgumentException("Password should contains at least 6 characters, including at least one lowercase letter, one uppercase letter and number.");
            }
            guestToUpdate.setPassword(guest.getPassword());
        }

        if (guest.getAddress() != null && !guest.getAddress().equals(guestToUpdate.getAddress())) {
            guestToUpdate.setAddress(guest.getAddress());
        }
        if (guest.getEmail() != null && !guest.getEmail().equals(guestToUpdate.getEmail())) {
            guestToUpdate.setEmail(guest.getEmail());
        }
        if (guest.getPhoneNumber() != null && !guest.getPhoneNumber().equals(guestToUpdate.getPhoneNumber())) {
            guestToUpdate.setPhoneNumber(guest.getPhoneNumber());
        }

        User user = userRepository.findByUsername(guestDTO.getLogin()).orElse(null);
        if (user != null) {
            user.setUsername(guestDTO.getLogin());
            user.setPassword(new BCryptPasswordEncoder().encode(guest.getPassword()));
        } else {
            userRepository.save(new User(guest.getLogin(),
                    new BCryptPasswordEncoder().encode(guest.getPassword()),
                    EnumRole.USER));
            Role userRole = roleRepository.findByName(EnumRole.USER).orElseThrow(() -> new NoSuchElementException("There is no Role for Guest"));
            roleService.addUserToRole(userRole,guest.getFirstName() + " " + guest.getLastName());
        }
        return guestMapper.toDto(guestRepository.save(guestToUpdate));
    }


    public List<GuestDTO> getAll() {
        return guestRepository.findAll().stream()
                .map(g -> guestMapper.toDto(g))
                .collect(Collectors.toList());
    }

    public GuestDTO getByLogin(String login) {
        return guestMapper.toDto(guestRepository.findByLogin(login).orElseThrow(() ->
                new NoSuchElementException("There is no guest with login: " + login)));
    }
}
