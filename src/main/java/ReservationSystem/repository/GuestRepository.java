package ReservationSystem.repository;

import ReservationSystem.data.entity.Guest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GuestRepository extends JpaRepository<Guest, Long> {
    Optional<Guest> findByFirstNameAndLastName(String name, String surname);

    @Override
    Guest save(Guest entity);

    Optional<Guest> findByLogin(String login);

    void deleteByLogin(String login);
}
