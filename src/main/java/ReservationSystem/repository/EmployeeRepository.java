package ReservationSystem.repository;

import ReservationSystem.data.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    Optional<Employee> findByUsername(String username);
    void deleteByFullName(String fullName);

    void deleteByUsername(String username);
    Optional<Employee>findByFullName(String fullName);
}
