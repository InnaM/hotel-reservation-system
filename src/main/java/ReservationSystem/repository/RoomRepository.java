package ReservationSystem.repository;

import ReservationSystem.data.dto.RoomDTO;
import ReservationSystem.data.entity.BedInfo;
import ReservationSystem.data.entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {

   Optional<Room> findByRoomNumber(String number);

   List<Room> findByBedInfo(BedInfo bedInfo);

   void deleteByRoomNumber(String roomNumber);

}
