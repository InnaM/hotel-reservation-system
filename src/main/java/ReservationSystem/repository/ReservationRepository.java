package ReservationSystem.repository;

import ReservationSystem.data.entity.BedInfo;
import ReservationSystem.data.entity.Reservation;
import ReservationSystem.data.entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, String> {

    Optional<Reservation> findByRoom_RoomNumberAndStartDateLessThanAndEndDateGreaterThanEqual(String roomNumber, LocalDateTime endDate, LocalDateTime startDate);
    List<Reservation> findByGuest_Login(String login);
    List<Reservation> findByGuest_FirstNameAndGuest_LastName(String firstName, String lastName);

    List<Reservation> findByRoom_BedInfoAndStartDateLessThanEqualAndEndDateGreaterThanEqual(BedInfo bedInfo, LocalDateTime endDate,LocalDateTime startDate );

    List<Reservation> findByStartDateLessThanEqualAndEndDateGreaterThanEqual (LocalDateTime endDate,LocalDateTime startDate);
}
