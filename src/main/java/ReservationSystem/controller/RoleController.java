package ReservationSystem.controller;

import ReservationSystem.data.entity.Role;
import ReservationSystem.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping("/auth/roles")
public class RoleController {
    private final RoleService roleService;

    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<Role> getRoles() {
        return roleService.allRoles();
    }
    @PostMapping("/add")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Role addRole(@RequestBody Role role) {
        return roleService.saveRole(role);
    }
    @GetMapping("/{name}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE')")
    public Role getRoleByName(@PathVariable String name) {
        return roleService.getRoleByName(name);
    }

    @DeleteMapping("/delete/{name}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Role deleteRoleByName(@PathVariable String name) {
        return roleService.deleteRoleByName(name);
    }

}
