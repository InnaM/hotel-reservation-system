package ReservationSystem.controller;

import ReservationSystem.data.dto.ReservationDTO;
import ReservationSystem.service.AvailableRoomRequest;
import ReservationSystem.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/reservations")
public class ReservationController {
    private final ReservationService reservationService;

    @Autowired
    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @PostMapping(value = "/addByAdmin")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE')")
    public ReservationDTO addReservationByAdmin(@RequestBody ReservationDTO reservationDTO) {
        return reservationService.addReservationByAdmin(reservationDTO);
    }

    @PostMapping(value="/addByUser")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_USER')")
    public ReservationDTO addReservationByUser(@RequestBody AvailableRoomRequest availableRoomRequest) {
        return reservationService.addReservationByUser(availableRoomRequest);
    }

    @DeleteMapping({"/delete/{id}"})
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_USER')")
    public ReservationDTO deleteById(@PathVariable String id) {
        return reservationService.deleteById(id);
    }

    @PutMapping({"/update/{id}"})
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_USER')")
    public ReservationDTO updateReservation(@RequestBody ReservationDTO reservationDTO) {
        return reservationService.updateReservation(reservationDTO);
    }

    @GetMapping("/get/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_USER') ")
    public ReservationDTO getReservationById(@PathVariable String id) {
        return reservationService.getById(id);
    }

    @GetMapping("/{username}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_USER') ")
    public List<ReservationDTO> getReservationByUsername(@PathVariable String username) {
        return reservationService.getByUsername(username);
    }

    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE')")
    public List<ReservationDTO> getAllReservations(@RequestParam(required = false) String reservationId,
                                                   @RequestParam(required = false) String roomNumber,
                                                   @RequestParam(required = false) String guestFirstName,
                                                   @RequestParam(required = false) String guestLastName) {
        return reservationService.getAllFiltered(reservationId, roomNumber, guestFirstName, guestLastName);
    }
}
