package ReservationSystem.controller;

import ReservationSystem.data.dto.GuestDTO;
import ReservationSystem.service.GuestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/guests")
public class GuestController {
    private final GuestService guestService;
    @Autowired
    public GuestController(GuestService guestService) {
        this.guestService = guestService;
    }

    @PostMapping({"/add"})
    //@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_USER')")
    public GuestDTO addGuest(@RequestBody GuestDTO guestDTO) {
        return guestService.addGuestByUser(guestDTO);
    }

    @DeleteMapping({"/delete/{login}"})
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_USER')")
    public GuestDTO deleteByLogin(@PathVariable String login) {
        return guestService.deleteByLogin(login);
    }

    @PutMapping({"/update"})
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_USER')")
    public GuestDTO updateGuest(@RequestBody GuestDTO guestDTO) {
        return guestService.updateGuest(guestDTO);
    }

    @GetMapping("/get/{login}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_USER')")
    public GuestDTO getGuestByLogin(@PathVariable String login) {
        return guestService.getByLogin(login);
    }

    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE')")
    public List<GuestDTO> getAllGuests() {
        return guestService.getAll();
    }
}
