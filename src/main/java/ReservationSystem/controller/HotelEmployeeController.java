package ReservationSystem.controller;

import ReservationSystem.data.dto.EmployeeDTO;

import ReservationSystem.service.EmployeeService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping("/auth/employees")
public class HotelEmployeeController {
    private final EmployeeService employeeService;

    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<EmployeeDTO> getEmployees() {
        return employeeService.getAllEmployees();
    }
    @PostMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public EmployeeDTO addEmployee(@RequestBody EmployeeDTO employeeDto) {
        return employeeService.addEmployee(employeeDto);
    }
    @GetMapping("/{username}")
    @PreAuthorize("hasRole('ROLE_ADMIN')  or hasRole('ROLE_EMPLOYEE')")
    public EmployeeDTO getEmployeeByUsername(@PathVariable String username) {
        return employeeService.getEmployeeByUserName(username);
    }
    @DeleteMapping("/delete/{username}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public EmployeeDTO deleteEmployeeByUsername(@PathVariable String username) {
        return employeeService.deleteByUsername(username);
    }

    @PutMapping("/update")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE')")
    public EmployeeDTO updateEmployee(@RequestBody EmployeeDTO employeeDto) {
        return employeeService.updateEmployee(employeeDto);
    }
}