package ReservationSystem.controller;

import ReservationSystem.data.dto.RoomDTO;
import ReservationSystem.service.AvailableRoomRequest;
import ReservationSystem.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RequestMapping("/rooms")
@RestController
public class RoomController {

    private final RoomService roomService;

    @Autowired
    public RoomController(RoomService roomService) {
        this.roomService = roomService;
    }

    @PostMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public RoomDTO addRoom(@RequestBody RoomDTO roomDTO) {
        return roomService.addRoom(roomDTO);
    }

    @DeleteMapping({"/{roomNumber}"})
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public RoomDTO deleteByNumber(@PathVariable String roomNumber) {
        return roomService.deleteByNumber(roomNumber);
    }

    @PutMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public RoomDTO updateRoomInfo(@RequestBody RoomDTO roomDTO) {
        return roomService.updateRoomInfo(roomDTO);
    }

    @GetMapping("/number/{number}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_USER') ")
    public RoomDTO getByRoomNumber(@PathVariable String number) {
        return roomService.getByRoomNumber(number);
    }

    @GetMapping()
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE')")
    public List<RoomDTO> getAllRoomsSorted(@RequestParam(required = false) String direction) {
        return roomService.getAll(direction);
    }

    @GetMapping("/bedInfo/{bedInfo}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE')")
    public List<RoomDTO> getAllByBedInfo(@PathVariable String bedInfo) {
        return roomService.getByBedInfo(bedInfo);
    }

    @PostMapping("/availableRooms")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE')")
    public List<RoomDTO> getAvailableRooms(@RequestBody AvailableRoomRequest availableRoomRequest) {
        return roomService.getAvailableRooms(availableRoomRequest);
    }

}
