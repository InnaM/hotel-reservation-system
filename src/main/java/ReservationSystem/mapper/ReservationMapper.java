package ReservationSystem.mapper;

import ReservationSystem.data.dto.ReservationDTO;
import ReservationSystem.data.entity.Reservation;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.*;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public interface ReservationMapper {

    @Mappings({
            @Mapping(source = "room.roomNumber", target = "roomNumber"),
            @Mapping(source = "guest.login", target = "guestLogin"),
            @Mapping(source = "guest.firstName", target = "guestFirstName"),
            @Mapping(source = "guest.lastName", target = "guestLastName"),
            @Mapping(source = "guest.email", target = "guestEmail"),
            @Mapping(source = "guest.address", target = "guestAddress"),
            @Mapping(source = "guest.phoneNumber", target = "guestPhoneNumber")
    })
    ReservationDTO toDto (Reservation reservation);

    @InheritInverseConfiguration
    Reservation fromDto (ReservationDTO reservationDto);
}
