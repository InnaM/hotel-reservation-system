package ReservationSystem.mapper;

import ReservationSystem.data.dto.GuestDTO;
import ReservationSystem.data.entity.Guest;
import org.mapstruct.*;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public interface GuestMapper {

    GuestDTO toDto (Guest guest);
    Guest fromDto(GuestDTO guestDTO);
}
