package ReservationSystem.mapper;

import ReservationSystem.data.dto.EmployeeDTO;
import ReservationSystem.data.entity.Employee;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public interface EmployeeMapper {

    EmployeeDTO toDto(Employee employee);
    Employee fromDto(EmployeeDTO employeeDTO);
}
