package ReservationSystem.mapper;

import ReservationSystem.data.dto.RoomDTO;
import ReservationSystem.data.entity.Room;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel="spring")
public interface RoomMapper {

    RoomDTO toDto (Room room);

    Room fromDto (RoomDTO roomDTO);

}
