package ReservationSystem.security.service;


import ReservationSystem.data.entity.EnumRole;
import ReservationSystem.data.entity.Role;
import ReservationSystem.data.entity.User;
import ReservationSystem.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {
    @Autowired
    private final UserRepository repository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
       return repository.findByUsername(username)
               .map(UserAdapter::new)
               .orElse(null);
    }

  public List<User> getAll( ){
       return repository.findAll();
  }


    public EnumRole getUserRole(String username) {
        User user = repository.findByUsername(username).orElseThrow(() ->
                new NoSuchElementException("There is no user with username: " + username));
        return user.getRole();
    }
}
