package ReservationSystem.security.service;


import ReservationSystem.data.entity.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RequiredArgsConstructor
public class UserAdapter implements UserDetails {
    private static final long serialVersionUID = -9099175240545719086L;
    private final User user;

    private final String PREFIX = "ROLE_";

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> adminAuthorities = null;
        String name = user.getRole().name();
        switch (name) {

            case "ADMIN":
                adminAuthorities = getAdminAuthorities();
                break;
            case "EMPLOYEE":
                adminAuthorities = getEmployeeAuthorities();
                break;
            case "USER":
                adminAuthorities = getUserAuthorities();
                break;
        }
        return adminAuthorities;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
    @JsonIgnore
    public List<SimpleGrantedAuthority> getAdminAuthorities() {
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority( PREFIX+"ADMIN");
        List<SimpleGrantedAuthority> authorityList = new ArrayList<>();
        authorityList.add(authority);

        return authorityList;
    }
    @JsonIgnore
    public List<SimpleGrantedAuthority> getUserAuthorities() {
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority(PREFIX+"USER");
        List<SimpleGrantedAuthority> authorityList = new ArrayList<>();
        authorityList.add(authority);

        return authorityList;
    }
    @JsonIgnore
    public List<SimpleGrantedAuthority> getEmployeeAuthorities() {
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority(PREFIX+"EMPLOYEE");
        List<SimpleGrantedAuthority> authorityList = new ArrayList<>();
        authorityList.add(authority);

        return authorityList;
    }


}
