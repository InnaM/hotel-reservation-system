package ReservationSystem.security.controller;

import ReservationSystem.security.model.JwtRequest;
import ReservationSystem.security.config.JwtService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@Slf4j
@RequiredArgsConstructor
public class AuthController {
    private final JwtService jwtService;

    private final AuthenticationManager authenticationManager;

    @PostMapping("/token")
    public String token(@RequestBody JwtRequest request) throws Exception {
        log.debug("Token requested for user: '{}' ", request.getUsername());
        Authentication authenticate = authenticate(request.getUsername(), request.getPassword());
        String token = jwtService.generateToken(authenticate);
        log.debug(" Token granted {}", token);
        return token;
    }

    private Authentication authenticate(String username, String password) throws Exception {
        try {
            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}
