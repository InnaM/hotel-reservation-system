package ReservationSystem.security.controller;

import ReservationSystem.data.entity.EnumRole;
import ReservationSystem.data.entity.User;
import ReservationSystem.security.service.UserService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
@Slf4j
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping ("/users")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<User> getUsers() {
        return userService.getAll();
    }

    @GetMapping ("/user/{username}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public UserDetails getUser(@PathVariable String username) {
        return userService.loadUserByUsername(username);
    }

    @GetMapping("/user/role/{username}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_USER')")
    public EnumRole getUserRole(@PathVariable String username) {
        return userService.getUserRole(username);
    }
}
