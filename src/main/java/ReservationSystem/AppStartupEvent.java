package ReservationSystem;

import ReservationSystem.data.dto.GuestDTO;
import ReservationSystem.data.dto.ReservationDTO;
import ReservationSystem.data.entity.Room;
import ReservationSystem.data.entity.User;
import ReservationSystem.mapper.ReservationMapper;
import ReservationSystem.repository.RoomRepository;
import ReservationSystem.repository.UserRepository;
import ReservationSystem.service.GuestService;
import ReservationSystem.service.ReservationService;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AppStartupEvent implements ApplicationListener<ApplicationReadyEvent> {

    private final RoomRepository roomRepository;

    private final ReservationService reservationService;

    private final GuestService guestService;

    private final ReservationMapper reservationMapper;

    private final UserRepository userRepository;


    public AppStartupEvent(RoomRepository roomRepository, ReservationService reservationService,
                           GuestService guestService, ReservationMapper reservationMapper, UserRepository userRepository) {
        this.roomRepository = roomRepository;
        this.reservationService = reservationService;
        this.guestService = guestService;
        this.reservationMapper = reservationMapper;
        this.userRepository = userRepository;
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        List<Room> rooms = roomRepository.findAll();
        rooms.forEach(r-> System.out.println("Room " +r.getRoomNumber()));

        List<ReservationDTO> reservations = reservationService.getAll();
        reservations.forEach(r-> System.out.println(r));

        List<GuestDTO> guests = guestService.getAll();
        guests.forEach(g-> System.out.println("Guest: "+ g.getFirstName()+ " "+ g.getLastName()));
        List<User> users = userRepository.findAll();
        users.forEach(user-> System.out.println(user));
    }
}
