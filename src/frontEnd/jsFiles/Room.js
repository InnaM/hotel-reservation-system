export class Room{
    constructor(roomNumber, bedInfo){
        this.roomNumber = roomNumber;
        this.bedInfo = bedInfo;
    }

    get roomNumber(){
        return this._roomNumber;
    }

    set roomNumber (roomNumber){
        this._roomNumber = roomNumber;
    }


    get bedInfo(){
        return this._bedInfo;
    }

    set bedInfo(bedInfo){
        this._bedInfo = bedInfo;
    }
}