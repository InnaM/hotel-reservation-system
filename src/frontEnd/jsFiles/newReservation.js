import {setUsernameInSubmenu, addAboutUsEvent,addReloadEvent, addReservationsEvent, addNewReservationEvent,
        addPhotosEvent, addNewsEvent, addContactsEvent, addToggleMenu,addEventToLogout,addEventToEditProfile, getMessage} from './menu.js'

import {Reservation} from './Reservation.js'



const username = localStorage.getItem("username");
const token = localStorage.getItem("token");
const role = localStorage.getItem("role");
console.log(username);


 if(role === "USER"){
    let userInput = document.getElementById("newReservationUser");
    if(userInput!==null){
        document.getElementById("newReservationUser").setAttribute('placeholder', username);
    }
}

let cancelBtn = document.getElementById("cancel_button");
if(cancelBtn!== null){
    document.getElementById("cancel_button").addEventListener("click", function(){
                                    window.location = "menu.html";
    });
}

var completeBtn = document.getElementById("complete_button");
if(completeBtn!== null){
    completeBtn.addEventListener("click", completeReservation);
}

export function getMyDateFormat(date){
    var parts = date.split('-');
    parts.reverse();
    var result = parts.join(".");
    return result;
}

export function getStartDate(){
    let start_input = document.getElementById("start_date").value;
    console.log(start_input);
    if(start_input!==""){
        let start_date = getMyDateFormat(start_input) + " 14:00";
        console.log(start_date);
        return start_date;
    }else {
         return "";
         }
}

export function getEndDate(){
    let end_input = document.getElementById("end_date").value;
    console.log(end_input);
    if(end_input!==""){
        let end_date = getMyDateFormat(end_input) + " 12:00";
        console.log(end_date);
        return end_date;
    } else {
    return "";
    }
}



async function completeReservation(){
    var user;
        if (role!=="USER"){
            user = document.getElementById("newReservationUser").value.toString();
        } else {
            user = localStorage.getItem("username");
        }

    let bedInfo= document.getElementById("bedInfo").value;
    console.log(bedInfo);
    let start_date = await getStartDate();
    let end_date = await getEndDate();
    console.log(end_date);
    if(bedInfo===""){
          getMessage("errorMsg","Please select Bed Info", "visible" );
          setTimeout(()=> { location.reload();}, 3000);
    } else if(start_date===""){
          getMessage("errorMsg","Wrong Arrive date", "visible" );
          setTimeout(()=> { location.reload();}, 3000);
    } else if(end_date===""){
         getMessage("errorMsg","Wrong Departure date", "visible" );
         setTimeout(()=> { location.reload();}, 3000);
    } else {
          let requestBody = {bedInfo : bedInfo,
                             username:user,
                             startDate:start_date,
                             endDate:end_date};
        console.log(requestBody);

        const result = await addReservation("http://localhost:8081/reservations/addByUser", requestBody,token);
        }
    }


async function addReservation(url, requestBody,token){
    try {
       const response = await fetch(url, {
                                         method: "POST",
                                         body: JSON.stringify(requestBody),
                                         headers: {
                                         "Authorization": token,
                                         "Content-Type": "application/json"}
                                         });

       if (!response.ok){
            const message = await response.text();
            console.error(message)
            await getMessage("errorMsg",message, "visible" );
           //setTimeout(()=> { location.reload();}, 3000);

        }
        const reservation = await response.json();
        console.log("Add one reservation: "+ reservation.reservationId );
        await showSuccessMsg(reservation.reservationId);
        setTimeout(()=> { window.location = "reservations.html";}, 3000);

        }
        catch(error){
            console.log(error);
        }
}
function showSuccessMsg(reservationId){
        getMessage("line1", "Your reservation was completed successfully.", "visible")
        getMessage("line2", "Your reservation id: "+ reservationId, "visible");
}


