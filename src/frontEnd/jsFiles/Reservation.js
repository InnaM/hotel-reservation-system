export class Reservation {

    constructor ( reservationId, roomNumber,
                   guestFirstName, guestLastName, guestEmail,
                   guestAddress, guestPhoneNumber,
                   startDate, endDate){
        this.reservationId = reservationId;
        this.roomNumber = roomNumber;
        this.guestFirstName = guestFirstName;
        this.guestLastName = guestLastName;
        this.guestEmail = guestEmail;
        this.guestAddress = guestAddress;
        this.guestPhoneNumber = guestPhoneNumber;
        this.startDate = startDate;
        this.endDate = endDate;
}

get reservationId (){
    return this._reservationId;
}

set reservationId (reservationId){
    if (reservationId.length > 0){
        this._reservationId = reservationId;
    } else {
        console.log("ReservationId couldn't be 0 or negative");
    }
}

get roomNumber (){
    return this._roomNumber;
}

set roomNumber (roomNumber){
    this._roomNumber = roomNumber;
}

get guestFirstName(){
    return this._guestFirstName;
}

set guestFirstName (guestFirstName){
    this._guestFirstName = guestFirstName;
}

get guestLastName(){
     return this._guestLastName;
}

 set guestLastName (guestLastName){
     this._guestLastName = guestLastName;
 }

get guestEmail(){
      return this._guestEmail;
}

set guestEmail (guestEmail){
      this._guestEmail = guestEmail;
}

get guestAddress(){
       return this._guestAddress;
}

set guestAddress (guestAddress){
       this._guestAddress = guestAddress;
}

get guestPhoneNumber(){
       return this._guestPhoneNumber;
}

set guestPhoneNumber (guestPhoneNumber){
       this._guestPhoneNumber = guestPhoneNumber;
}

get startDate (){
    return this._startDate;
}
set startDate (startDate){
    this._startDate = startDate;
}

get endDate (){
    return this._endDate;
}
set endDate (endDate){
    this._endDate = endDate;
}
}