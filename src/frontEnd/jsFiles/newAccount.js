import {Guest} from './Guest.js'


addCreateBtnListener();
addCancelBtnListener();



async function createAccount(){
let requestBody = createRequestData();
console.log(requestBody);
await addGuest("http://localhost:8081/guests/add", requestBody);

}

function addCreateBtnListener(){
    var newAccBtn = document.getElementById("account_complete_button");
    newAccBtn.addEventListener("click", createAccount);
}

function addCancelBtnListener(){
    var cancelBtn = document.getElementById("account_cancel_button");
    cancelBtn.addEventListener("click", function(){
     window.location = "index.html";
    });
}


function getInputText(inputId){
    let inputText = document.getElementById(inputId).value;
    console.log(inputText);
    return inputText;
}

function createRequestData(){
let firstName = getInputText("firstname");
let lastName = getInputText("lastname");
let login = getInputText("username");
let password = getInputText("password");
let email = getInputText("email");
let address = getInputText("address");
let phoneNumber = getInputText("phone");

var guest = {login:login, firstName:firstName, lastName:lastName,
                    password:password,email:email,
                    address:address, phoneNumber:phoneNumber};
console.log(guest);
return guest;
}

async function addGuest(url, requestBody){
    try {
    const response = await fetch(url, {
                                  method: "POST",
                                  body: JSON.stringify(requestBody),
                                  headers: {"Content-Type": "application/json"}
                                  });
    if (!response.ok){
                const message = await response.text();
                console.error(message)
                await getMessage("errorMsg",message, "visible" );
                setTimeout(()=> { location.reload();}, 4000);

            }
            const guest = await response.json();
            console.log("Your account was created successfully");
            await getMessage("successMsg", "Your account was created successfully", "visible" );
            setTimeout(()=> { window.location = "index.html";}, 3000);

    }
    catch(error){
        console.log(error);
    }
 }

 function getMessage(id, text, visibility){
         const line = document.getElementById(id);
         line.textContent = text;
         line.style.visibility = visibility;
  }
