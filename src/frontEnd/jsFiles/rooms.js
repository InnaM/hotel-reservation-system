import {setUsernameInSubmenu, addAboutUsEvent,addReloadEvent, addReservationsEvent, addNewReservationEvent,
        addPhotosEvent, addNewsEvent, addContactsEvent, addToggleMenu,addEventToLogout,addEventToEditProfile, getMessage, dynamicUpdateForm } from './menu.js'

import {Reservation} from './Reservation.js'
import {Room} from './Room.js'


const username = localStorage.getItem("username");
const token = localStorage.getItem("token");
var start_date="";
var end_date="";
var roomNumbers = [];

addFindEvent();
addReservationBtnEvent();
addCompleteBtnEvent();
addCancelBtnEvent();
dynamicUpdateForm("user-form-container");

function addFindEvent(){
    document.getElementById("find_button").addEventListener("click", findAvailableRooms);
}

function addReservationBtnEvent(){
 document.getElementById("addBtn").addEventListener("click", getUserForm);
}

function addCompleteBtnEvent(){
 document.getElementById("room_complete_button").addEventListener("click", addReservation);
}

function addCancelBtnEvent(){
 document.getElementById("room_cancel_button").addEventListener("click", closeMenu);
}


async function findAvailableRooms(){
    let requestBody = await getInputData();
    console.log(requestBody);

    if(requestBody!== ""){
        const rooms = await findRooms("http://localhost:8081/rooms/availableRooms", requestBody,token);
        if(rooms.length > 0){
            console.log("Rooms list size(): "+ rooms.length)
            await displayRoomsTable(rooms);
        } else {
            await getMessage("room_errorMsg", "There are no available rooms in such period.", "visible" );
                       setTimeout(()=> { location.reload();}, 3000);
        }
    } else {
        setTimeout(()=> { location.reload();}, 3000);
    }
}

async function getInputData(){
    let bedInfo = document.getElementById("bedInfo").value;
    if(bedInfo === ""){
        bedInfo = null;
    }
    console.log(bedInfo);
    let isValidDate = await getStartAndEndDate();
    if(!isValidDate){
       return "";
    } else {

    let requestBody = {bedInfo : bedInfo,
                       username:username,
                       startDate:start_date,
                       endDate:end_date};
    console.log(requestBody);

    return requestBody;
    }
}

function getMyDateFormat(date){
    var parts = date.split('-');
    parts.reverse();
    var result = parts.join(".");
    return result.toString();
}

async function getStartAndEndDate(){
    let start_input = getDate("start_date");
    let end_input = getDate("end_date");
      console.log(start_input + " "+end_input);

    let startDate = new Date(start_input);
    let endDate = new Date(end_input);
    if(startDate.valueOf() > endDate.valueOf()){
        console.error("Departure day couldn't be before arrive day");
        await getMessage("room_errorMsg", "Departure day couldn't be before arrive day", "visible" );
        return false;
    } else if(start_input === "" || end_input === "") {
            console.error("Empty Arrive or Departure Date");
            await getMessage("room_errorMsg", "Empty Arrive or Departure Date", "visible" );
            return false;
    } else {
        start_date = getMyDateFormat(start_input) + " 14:00";
        console.log(start_date);
        end_date = getMyDateFormat(end_input) + " 12:00";
        console.log(end_date);
        return true;
    }
}

function getDate(inputId){
    let date_input = document.getElementById(inputId).value;
    console.log(date_input);
    return date_input;
}


async function findRooms(url, requestBody,token){
     try {
       const response = await fetch(url, {
                                         method: "POST",
                                         body: JSON.stringify(requestBody),
                                         headers: {
                                         "Authorization": token,
                                         "Content-Type": "application/json"}
                                         });

       if (!response.ok){
           const message = await response.text();
           console.error(message);
           await getMessage("room_errorMsg",message, "visible" );
           setTimeout(()=> { location.reload();}, 4000);

        }
        const rooms = await response.json();
        console.log("Fetch available rooms list from DB: " + rooms.length );
        return rooms;
        }
        catch(error){
            console.log(error);
        }
}

async function displayRoomsTable(rooms){

    const rooms_table = document.getElementById("rooms_table");
    await cleanTable(rooms_table);
    console.log("////////"+rooms_table.children.length);
    await openMenu("table-form-container");
    await appendRoomList(rooms, rooms_table);
    console.log("Finish display rooms.")
}

async function cleanTable(rooms_table){
    console.log("////////"+rooms_table.children.length);
    while (rooms_table.children.length > 1) {
        rooms_table.removeChild(rooms_table.lastChild);
        console.log("////////"+rooms_table.children.length);
    }
}

async function appendRoomList(rooms, rooms_table){
      if (rooms.length > 0){
        for (let room of rooms){
            const values = Object.values(room);
            console.log(values);
            appendRoom(values, rooms_table);
        }
    }
}

function appendRoom(values, rooms_table){
   console.log("start append room");
   var tr = document.createElement('tr');
   tr.id="tr_"+values[0];

   var input = document.createElement('input');
   input.type = "checkbox";
   input.class = "checkbox";
   input.id = values[0];

   var td0 = document.createElement('td');
   var td1 = document.createElement('td');
   var td2 = document.createElement('td');
   var td3 = document.createElement('td');
   var td4 = document.createElement('td');

   var text1 = document.createTextNode(values[0]);
   var text2 = document.createTextNode(values[1]);
   var text3 = document.createTextNode(start_date);
   var text4 = document.createTextNode(end_date);

   td0.appendChild(input);
   td1.appendChild(text1);
   td2.appendChild(text2);
   td3.appendChild(text3);
   td4.appendChild(text4);

   tr.appendChild(td0);
   tr.appendChild(td1);
   tr.appendChild(td2);
   tr.appendChild(td3);
   tr.appendChild(td4);

   rooms_table.appendChild(tr);
   console.log("append one more room");
}

 async function getRoomIdToCreateReservation(){
    let roomIds = [];
    let count = 0;
    let checkedBoxes = document.querySelectorAll('input[type="checkbox"]');
        for (let i=0; i<checkedBoxes.length; i++){
            if (checkedBoxes[i].checked == true){
                roomIds[count] = checkedBoxes[i].id;
                count++;
            }
        }
    return roomIds;
 }

 async function getUserForm(){
    let roomIds = await getRoomIdToCreateReservation();
    console.log("Checked RoomNumbers: " + roomIds);
    if (roomIds.length > 0){
        let tableElement = document.getElementById("table-form-container");
        tableElement.style.opacity=0.5;
        openMenu("user-form-container");
        roomNumbers = roomIds;
    }
 }

 async function addReservation(){
     let addedReservationsId=[];

     for(let roomNumber of roomNumbers){
          let requestBody = await getGuestInputData(roomNumber);
          let id = await addReservationByAdmin("http://localhost:8081/reservations/addByAdmin", requestBody,token);
          if(id!==""){
          addedReservationsId.push(id);
          console.log("Added id: "+ id);
          }
      }

     if(addedReservationsId.length > 0){
     console.log("Length of added reservations: "+ addedReservationsId.length);
        if(addedReservationsId.length == roomNumbers.length){
                await getMessage("guest_successMsg", "All reservations were created successfully", "visible");
                setTimeout(()=> { location.reload();}, 3000);
            } else {
                await getMessage("guest_successMsg", "Number of created reservations: "+ addedReservationsId.length, "visible");
                setTimeout(()=> { location.reload();}, 3000);
            }
     }
  }

 async function addReservationByAdmin(url, requestBody, token){
     try {
          const response = await fetch(url, {
                                             method: "POST",
                                             body: JSON.stringify(requestBody),
                                             headers: {
                                             "Authorization": token,
                                             "Content-Type": "application/json"}
                                             });
          if (!response.ok){
              const message = await response.text();
              console.error(message);
              await getMessage("guest_errorMsg", message, "visible" );
              setTimeout(()=> { getMessage("guest_errorMsg","", "hidden");}, 3000);
                return "";
             }
          const reservation = await response.json();
          console.log("Create reservation: " + reservation.reservationId);
           return reservation.reservationId;

          } catch(error){
              console.log(error);
          }
  }

 async function getGuestInputData(roomNumber){
     let guestFirstName = document.getElementById("firstname").value;

     let guestLastName = document.getElementById("lastname").value;

     let guestEmail = document.getElementById("email").value;
     console.log( "Email"+ guestEmail);

     let guestAddress = document.getElementById("address").value;
          console.log( "Address"+ guestAddress);

     let guestPhoneNumber = document.getElementById("phone").value;
                    console.log( "Phone"+ guestPhoneNumber);

     let requestBody = {roomNumber : roomNumber,
                        guestFirstName:guestFirstName,
                        guestLastName: guestLastName,
                        guestEmail: guestEmail,
                        guestAddress: guestAddress,
                        guestPhoneNumber: guestPhoneNumber,
                        startDate:start_date,
                        endDate:end_date};
     console.log(requestBody);
     return requestBody;
 }

 async function openMenu(elementId){
     let subMenu = document.getElementById(elementId);
     subMenu.classList.add("open_guest_data_menu");
 }

  function appendMessage(id, text, visibility){
          const msg = document.getElementById(id);
          msg.textContent += text.concat(", ");
          msg.style.visibility = visibility;
  }

  async function closeMenu(){
      let subMenu = document.getElementById("user-form-container");
      subMenu.classList.remove("open_guest_data_menu");
      let tableElement = document.getElementById("table-form-container");
              tableElement.style.opacity=1;
  }











