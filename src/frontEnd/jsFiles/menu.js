setUsernameInSubmenu();
addAboutUsEvent();
addReservationsEvent();
addPhotosEvent();
addNewsEvent();
addContactsEvent();
addEventToEditProfile();
addEventToLogout();
addReloadEvent();
addToggleMenu();
addNewReservationEvent();

let welcome = document.getElementById("welcome");
if(welcome!== null){
    setTimeout(()=> { welcome.style.display= "none";}, 5000);
    setTimeout(displayWeather, 5000);
}

export function addAboutUsEvent(){
    document.getElementById("aboutUsBtn").addEventListener("click",
        function(){
            window.location = "aboutUs.html";
        });
}


export function addNewReservationEvent(){
    const role = localStorage.getItem("role");
    let element = document.getElementById("newReservationBtn");
    if(role === "USER"){
      element.addEventListener("click", function(){
                         window.location = "newReservation.html";
                     });
     } else {
      element.addEventListener("click", function(){
                        window.location = "rooms.html";
                          });
     }
}


export function addReservationsEvent(){
    document.getElementById("reservationsBtn").addEventListener("click",
        function(){
            window.location = "reservations.html";
        });
}

export function addPhotosEvent(){
    document.getElementById("photosBtn").addEventListener("click",
        function(){
            window.location = "photos.html";
    });
}
export function addNewsEvent(){
    document.getElementById("newsBtn").addEventListener("click",
        function(){
            window.location = "news.html";
    });
}

export function addContactsEvent(){
    document.getElementById("contactBtn").addEventListener("click",
         function(){
            window.location = "contact.html";
    });
}

export function setUsernameInSubmenu(){
   let subMenu = document.getElementById("userSubMenu");
    const username = localStorage.getItem("username");
    let usernameElement = document.getElementById("username");
    usernameElement.textContent = username;
}


export function addReloadEvent(){
    document.getElementById("hotel_logo").addEventListener("click",
         function(){
            window.location = "menu.html";
    });
}

export function addToggleMenu(){
    let profileLogo = document.getElementById("profile_logo");
    let subMenu = document.getElementById("userSubMenu");
    console.log(profileLogo);
    profileLogo.addEventListener("click",
         function(){
            subMenu.classList.toggle("open_menu");
    });
}

export function addEventToLogout(){
    document.getElementById("logout").addEventListener("click",
        function(){
            localStorage.removeItem("username");
            localStorage.removeItem("password");
            localStorage.removeItem("token");
            window.location = "index.html";
    });
}

export function addEventToEditProfile(){
var element = document.getElementById("edit");
console.log(element);
    document.getElementById("edit").addEventListener("click",
        function(){
           window.location = "editProfile.html";
    });
}

export async function getMessage(id, text, visibility){
         const msg = document.getElementById(id);
         msg.textContent = text;
         msg.style.visibility = visibility;
 }


 async function displayWeather(){
    await getIntervalData();
    setInterval(getIntervalData, 360000);

 }

 async function getIntervalData(){
          console.log("Get weather!");
          let weatherData = await getData();
          console.log(weatherData);

         if(weatherData!==null){
               await displayDate(weatherData);
               document.getElementById("weather").style.display= "flex";
         }
 }

 async function getData(){
      const url = "http://api.weatherapi.com/v1/current.json?key=0621bf58f62e45febd5160544242207&q=Krabi";

          const response = await fetch(url);
          if (!response.ok){
             const message = await response.text();
             console.error(message);
             console.log(message);
             return null;
          }
          const data = await response.json();
          const values = Object.values(data);
          console.log("Fetch weather: " + values[1].temp_c);
          return values[1];
    }

    async function displayDate(current){
        document.getElementById("temperature").textContent = current.temp_c+"°C";
        document.getElementById("humidity").textContent = "Humidity: "+ current.humidity+"%";
        var condition = Object.values(current.condition);
        console.log(condition[0]);
        document.getElementById("description").textContent = condition[0];
        let imageSrc = condition[1];
        console.log(imageSrc);
        document.getElementById("weatherImg").src = imageSrc;
 }

 export async function dynamicUpdateForm(elementId){
    let offSetX = 0;
    let offSetY = 0;
    let mouseX = 0;
    let mouseY = 0;
    let isMouseDown = false;

      const element = document.getElementById(elementId);
      console.log("Adding Drag&Drop to element id: "+element.id);
     if (element!== null){
         element.addEventListener('mousedown', (e)=>{
                         console.log("Element size: "+ element.offsetWidth,element.offsetHeight )
                         offSetX = e.clientX - element.offsetLeft ;
                         console.log("offsetX: "+ offSetX, e.clientX);

                         offSetY = e.clientY - element.offsetTop;
                         console.log("offsetY: "+ offSetY,e.clientY );

                         if( offSetX < element.offsetWidth-10 && offSetY< element.offsetHeight-10){
                             isMouseDown = true;
                         }

         });

         document.addEventListener('mousemove', (e)=>{
              if(!isMouseDown) return;
               e = e || window.event;
               e.preventDefault();
               mouseX = e.clientX - offSetX;
               mouseY = e.clientY - offSetY;
               if(mouseX<0 || mouseX > window.innerWidth - offSetX){
                 mouseX=mouseX < 0 ? 0 : window.innerWidth - offSetX;
               }

               if(mouseY<0 || mouseY > window.innerHeight - offSetY){
                  mouseY = mouseY < 0 ? 0 : window.innerHeight - element.offSetY;
               }
               element.style.left = mouseX+'px';
               element.style.top = mouseY+'px';
               });

         document.addEventListener("mouseup",  (e)=>{
         console.log("New position: "+ mouseX, mouseY);
            isMouseDown = false;
         });
     }
 }








