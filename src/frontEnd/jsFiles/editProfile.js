import {setUsernameInSubmenu, addAboutUsEvent,addReloadEvent, addReservationsEvent, addNewReservationEvent,
        addPhotosEvent, addNewsEvent, addContactsEvent, addToggleMenu,addEventToLogout,addEventToEditProfile } from './menu.js'

import {Guest} from './Guest.js'
import {Employee} from './Employee.js'


const username = localStorage.getItem("username");
const token = localStorage.getItem("token");
const role = localStorage.getItem("role");
console.log(username);
document.getElementById("edit_username").textContent=username;


var cancelBtn = document.getElementById("cancel");
cancelBtn.addEventListener("click", function(){
                                    window.location ="menu.html";}
                                    );

var updateBtn = document.getElementById("update_account");
updateBtn.addEventListener("click", updateAccount);

getUserData(username);

var updateResult;
var result;

async function getUserData(username){

    if(role === "USER"){
        result = await getProfileByUsername("http://localhost:8081/guests/get/"+username, token);
        console.log(result);
        await setInputPlaceholder(result);
    } else{
        result = await getProfileByUsername("http://localhost:8081/auth/employees/"+username, token);
        await setInputPlaceholder(result);
        }
}


async function setInputPlaceholder(result){
    if(role === "USER"){
        document.getElementById("firstname").setAttribute('placeholder', result.firstName);
        document.getElementById("lastname").setAttribute('placeholder', result.lastName);
        document.getElementById("email").setAttribute('placeholder', result.email);
        document.getElementById("address").setAttribute('placeholder', result.address);
        document.getElementById("phone").setAttribute('placeholder', result.phoneNumber);

    } else{
         var fullNameArray=result.fullName.split(" ");
         document.getElementById("firstname").setAttribute('placeholder', fullNameArray[0]);
         document.getElementById("lastname").setAttribute('placeholder', fullNameArray[1]);
         document.getElementById("email").disabled=true;
         document.getElementById("address").disabled=true;
         document.getElementById("phone").disabled=true;
    }
}



async function getProfileByUsername(url, token){
    try {
       const response = await fetch(url, {
                                         method: "GET",
                                         headers: {
                                         "Authorization": token,
                                         "Content-Type": "application/json"}
                                         });

       if (!response.ok){
            const message = await response.text();
            console.error(message)
        }
        console.log(response );
        const result = await response.json();
        console.log("Fetch profile: "+ result.login );
        return result;
        }
        catch(error){
            console.log(error);
        }
}

async function getGuestFromInput(){
console.log("Start get input data");
    var firstName = getInputText("firstname");
    if(firstName === ""){
        firstName = result.firstName;
    }
    var lastName =  getInputText("lastname");

    if(lastName === ""){
        lastName = result.lastName;
    }

    var newPassword = getInputText("password");
    if(newPassword === ""){
        newPassword = localStorage.getItem("password");
    }
    var email = getInputText("email");
    if(email ===""){
         email = result.email;
    }
    var address =  getInputText("address");
    if(address === ""){
         address = result.address;
    }

    var phone =  getInputText("phone");
    if(phone === ""){
        phone = result.phoneNumber;
    }

    var guest = {login:username, firstName:firstName, lastName:lastName,
                        password:newPassword,email:email,
                        address:address, phoneNumber:phone};
    console.log(guest);
    return guest;
}

async function getEmployeeFromInput(){
    var fullNameArray=result.fullName.split(" ");

     var firstName = getInputText("firstname");

        if(firstName === ""){
            firstName = fullNameArray[0];
        }

        var lastName =  getInputText("lastname");
        if(lastName === ""){
            lastName = fullNameArray[1];
        }

        var newPassword = getInputText("password");
        if(newPassword === ""){
            newPassword = localStorage.getItem("password");
        }
        var fullName = firstName.concat(" ").concat(lastName);
 var employee = {username:username, fullName:fullName,
                        password:newPassword};
    console.log(employee);
return employee;
}

async function updateAccount(){
var updateResult;
    if(role === "USER"){
        var guest = await getGuestFromInput();
        console.log(guest);
        if (guest !== result){
            updateResult = await updateProfile("http://localhost:8081/guests/update", token, guest);
            await document.getElementById("successMsg");
        }
    } else{
        var employee = await getEmployeeFromInput();
        console.log(employee);
        if (!employee !== result){
        updateResult = await updateProfile("http://localhost:8081/auth/employees/update", token, employee);
        }
    }
}

async function updateProfile(url, token, profile){
    try {
       const response = await fetch(url, {
                                         method: "PUT",
                                         body: JSON.stringify(profile),
                                         headers: {
                                         "Authorization": token,
                                         "Content-Type": "application/json"}
                                         });

       if (!response.ok){
            const message = await response.text();
            console.error(message)
            await getMessage("errorMsg",message, "visible" );
            setTimeout(()=> {getMessage("errorMsg","", "hidden" );}, 3000);
        }
        console.log(response );
        const result = await response.json();
        console.log("Update profile: "+ result );
        await getMessage("successMsg","Your data were changed successfully.", "visible" );
        setTimeout(()=> { window.location = "index.html";}, 3000);
        return result;
        }
        catch(error){
            console.log(error);
        }
}

async function getMessage(id, text, visibility){
        const line = document.getElementById(id);
        line.textContent = text;
        line.style.visibility = visibility;
 }
function getInputText(inputId){
     let inputText = document.getElementById(inputId).value;
     console.log(inputText + ""+inputText.length);
     return inputText;
 }