export class Guest{

constructor ( login, firstName, lastName, password,email, address, phoneNumber){
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.email = email;
        this.address = address;
        this.phoneNumber = phoneNumber;
}

get login(){
    return this._login;
}

set login(login){
        this._login = login;
}

get firstName(){
    return this._firstName;
}

set firstName(firstName){
        this._firstName = firstName;
}

get lastName(){
    return this._lastName;
}

set lastName(lastName){
        this._lastName = lastName;
}

get password(){
    return this._password;
}

set password(password){
        this._password = password;
}

get email(){
    return this._email;
}

set email(email){
        this._email = email;
}

get address(){
    return this._address;
}

set address(address){
        this._address = address;
}

get phoneNumber(){
    return this._phoneNumber;
}

set phoneNumber(phoneNumber){
        this._phoneNumber = phoneNumber;
}

}