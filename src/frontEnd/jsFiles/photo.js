import {setUsernameInSubmenu, addAboutUsEvent,addReloadEvent, addReservationsEvent, addNewReservationEvent,
        addPhotosEvent, addNewsEvent, addContactsEvent, addToggleMenu,addEventToLogout, addEventToEditProfile  } from './menu.js'

const photos = document.querySelectorAll(".photos img");
console.log(photos.length);
let photoIdx = 0;
let intervalId = null;
addPrevPhotoEvent();
addNextPhotoEvent();


document.addEventListener("DOMContentLoaded", initPhotos);

function initPhotos(){
    if(photos.length > 0){
        photos[photoIdx].classList.add("displayPhoto");
        intervalId = setInterval(nextPhoto, 3000);
        console.log(intervalId);
        }
}

function showPhoto(index){
    if(index >= photos.length){
        photoIdx = 0;
    } else if (index < 0){
        photoIdx = photos.length-1;
    }
    photos.forEach(photo => {
        photo.classList.remove("displayPhoto");
    });
    photos[photoIdx].classList.add("displayPhoto");
}
function addPrevPhotoEvent(){
    let prevBtn = document.getElementById("prev");
    console.log(prevBtn.id);
    prevBtn.addEventListener("click",prevPhoto);
}
function addNextPhotoEvent(){
    let nextBtn = document.getElementById("next");
        console.log(nextBtn.id);
        nextBtn.addEventListener("click",nextPhoto);
}

function prevPhoto(){
    clearInterval(intervalId);
    photoIdx--;
    console.log(photoIdx);
    showPhoto(photoIdx);
}

function nextPhoto(){
    photoIdx++;
    console.log(photoIdx);
    showPhoto(photoIdx);
}
