import {setUsernameInSubmenu, addAboutUsEvent,addReloadEvent, addReservationsEvent, addNewReservationEvent,
        addPhotosEvent, addNewsEvent, addContactsEvent, addToggleMenu,addEventToLogout, addEventToEditProfile, getMessage, dynamicUpdateForm} from './menu.js'

import {Reservation} from './Reservation.js'

import {getMyDateFormat,getStartDate, getEndDate } from './newReservation.js'

const username = localStorage.getItem("username");
const password = localStorage.getItem("password");
const token = localStorage.getItem("token");
const role = localStorage.getItem("role");
console.log(username);
console.log(password);

var filterReservationId="";
var filterRoomNumber = "";
var filterFirstName = "";
var filterLastName = "";

var reservationIdToUpdate ="";

displayUserReservations(token, );
addCancelEvent();
addUpdateEvent();
addFilterEvent();
openFilter();
dynamicUpdateForm("reservation-form-container");

async function displayUserReservations(token){
    var userReservations;
    if (role === "USER"){
        userReservations = await getData("http://localhost:8081/reservations/"+ username, token);
        console.log("List of reservation for the user: "+ userReservations);
    } else {
        userReservations = await getData(
            "http://localhost:8081/reservations", token);
    }
    await displayReservationsTable(userReservations);
    await addEditReservationEvent();
}

async function openFilter(){
    if (role !== "USER"){
    console.log("This is NOT GUEST!!!");
    let filerForm = document.querySelector(".filter-form-container");
    if(filerForm!==null){
        filerForm.classList.add("open-form");
        console.log("Add filter div");
        }
    }
}

async function getData(url, token){
     try {
        const response = await fetch(url, {
                                   method: "GET",
                                   headers: {
                                   "Authorization": token, "Accept":"application/json"}
                                   });
        if (!response.ok){
         throw new Error("Could not fetch resource");
        }
        const reservationList = await response.json();
        console.log("Fetch reservation list from DB: " + reservationList );
        return reservationList;
     }
     catch(error){
       console.error(error)
     }
}

function displayReservationsTable(userReservations){
    const reservations_table = document.getElementById("reservation_table");
    console.log(reservations_table);

    if (userReservations.length > 0){
        for (let reservation of userReservations){
            const values = Object.values(reservation);
            console.log(values);
            appendReservation(values, reservations_table);
        }
    }
    console.log("Finish display reservations.")
}

function appendReservation(values, reservations_table){
   console.log("start append child");
   var tr = document.createElement('tr');
   tr.id="tr_"+values[0];

   var input = document.createElement('input');
   input.type = "checkbox";
   input.class = "checkbox";
   input.id = values[0];

   var td0 = document.createElement('td');
   var td1 = document.createElement('td');
   var td2 = document.createElement('td');
   var td3 = document.createElement('td');
   var td4 = document.createElement('td');
   var td5 = document.createElement('td');
   var td6 = document.createElement('td');
   var td7 = document.createElement('td');
   var td8 = document.createElement('td');
   var td9 = document.createElement('td');


   var text1 = document.createTextNode(values[0]);
   var text2 = document.createTextNode(values[1]);
   var text3 = document.createTextNode(values[2] + " "+ values[3]);
   var text4 = document.createTextNode(values[4]);
   var text5 = document.createTextNode(values[5]);
   var text6 = document.createTextNode(values[6]);
   var text7 = document.createTextNode(values[7]);
   var text8 = document.createTextNode(values[8]);


   td0.appendChild(input);
   td1.appendChild(text1);
   td2.appendChild(text2);
   td3.appendChild(text3);
   td4.appendChild(text4);
   td5.appendChild(text5);
   td6.appendChild(text6);
   td7.appendChild(text7);
   td8.appendChild(text8);
   td9.innerHTML = '<img src ="images/edit.png" class="editBtn" id='+values[0]+' />';

   tr.appendChild(td0);
   tr.appendChild(td1);
   tr.appendChild(td2);
   tr.appendChild(td3);
   tr.appendChild(td4);
   tr.appendChild(td5);
   tr.appendChild(td6);
   tr.appendChild(td7);
   tr.appendChild(td8);
   tr.appendChild(td9);

   reservations_table.appendChild(tr);
   console.log("append one more child");
}

function newReservationWindow(){
    window.location = "newReservation.html";
}

var removeBtn = document.getElementById("removeBtn");
removeBtn.addEventListener("click", removeReservation);

async function removeReservation(){

function getIdToRemove(){
    let reservationIds = [];
    let count = 0;
    let checkedBoxes = document.querySelectorAll('input[type="checkbox"]');
    console.log(checkedBoxes);
    for (let i=0; i<checkedBoxes.length; i++){
        if (checkedBoxes[i].checked == true){
        reservationIds[count] = checkedBoxes[i].id;
        count++;
        }
    }
    return reservationIds;
}

const reservationIds = await getIdToRemove();
console.log(reservationIds);

await removeReservations("http://localhost:8081/reservations/delete/", reservationIds, token);
}

async function removeReservations(url, listOfIds,token){
    for (let id of listOfIds){
        await removeReservationById(url, id, token);
        let elementToDelete = await document.getElementById("tr_"+id);
        console.log(elementToDelete);
        await elementToDelete.parentNode.removeChild(elementToDelete);
        console.log("Removed one object from the TABLE with id: "+id);
    }
    console.log("Finish remove reservations");
}

async function removeReservationById(url, id, token){
    try {
        const response = await fetch(url.concat(id), {
                                      method: "DELETE",
                                      headers: {
                                      "Authorization": token, "Accept":"application/json"}
                                      });
        if (!response.ok){
            throw new Error("Could not get response");
        }

        const deletedReservation = await response.json();
        console.log("Remove one object with id: "+id );
        return deletedReservation;
        }
        catch(error){
          console.error(error)
        }
}

async function addEditReservationEvent(){
    var editBts = document.querySelectorAll(".editBtn");
    console.log(editBts);
    if(editBts.length>0){
        editBts.forEach(el =>
            el.addEventListener("click", event => editReservation(event)));
    }
}

async function editReservation(event){
     reservationIdToUpdate = event.target.id;
     console.log(reservationIdToUpdate);
     let tableElement = document.getElementById("table_cont");
     tableElement.style.opacity=0.5;

     await setInputPlaceholder(reservationIdToUpdate);
     await openMenu("reservation-form-container");
}

async function setInputPlaceholder(reservationId){
    var cells = document.getElementById("tr_"+reservationId).childNodes;
    console.log(cells[3].innerHTML);
    document.getElementById("reservationId").innerHTML=reservationId;
    let roomNumber =  document.getElementById("roomNumber");
    roomNumber.value = cells[2].innerHTML;
    roomNumber.disabled = true;
    let fullNameTable = cells[3].innerHTML.split(' ');
    document.getElementById("firstname").setAttribute('placeholder', fullNameTable[0]);
    document.getElementById("lastname").setAttribute('placeholder', fullNameTable[1]);
    document.getElementById("email").setAttribute('placeholder', cells[4].innerHTML);
    document.getElementById("address").setAttribute('placeholder', cells[5].innerHTML);
    document.getElementById("phone").setAttribute('placeholder', cells[6].innerHTML);
}

 async function openMenu(elementId){
     let subMenu = document.getElementById(elementId);
     subMenu.classList.add("open_data_menu");
 }

 async function addUpdateEvent(){
     document.getElementById("reservation_update_button").addEventListener("click", updateReservation);
 }


 async function updateReservation(){
    let roomNumber = document.getElementById("roomNumber").value;
    let guestFirstName = getInput("firstname");
    let guestLastName = getInput("lastname");
    let guestEmail = getInput("email");
    let guestAddress = getInput("address");
    let guestPhoneNumber = getInput("phone");

    let startDate = await getStartDate();
    console.log(startDate);
    let endDate = await getEndDate();
    console.log(endDate);
    if(startDate===""){
              getMessage("res_errorMsg","Wrong Arrive date", "visible" );
              setTimeout(()=> { getMessage("res_errorMsg","", "hidden");}, 3000);
    } else if(endDate===""){
             getMessage("res_errorMsg","Wrong Departure date", "visible" );
             setTimeout(()=> { getMessage("res_errorMsg","", "hidden");}, 3000);
    } else {
        let requestBody = {reservationId:reservationIdToUpdate, roomNumber,
        guestFirstName, guestLastName, guestEmail, guestAddress,
        guestPhoneNumber, startDate, endDate};
        console.log(requestBody);
        const result = await update("http://localhost:8081/reservations/update/"+reservationIdToUpdate, requestBody, token);
    }
}

async function update(url, requestBody,token){
    try {
       const response = await fetch(url, {
                                          method: 'PUT',
                                          body: JSON.stringify(requestBody),
                                          headers: {
                                          "Authorization": token,
                                          "Content-Type": "application/json"}
                                          });

       if (!response.ok){
            const message = await response.text();
            console.error(message)
            await getMessage("res_errorMsg",message, "visible" );
            setTimeout(()=> { getMessage("res_errorMsg","", "hidden");}, 10000);
       } else{
            const reservation = await response.json();
            console.log("Update reservation: " + reservation.reservationId);
            await getMessage("res_successMsg", "Reservations was updated successfully: "+reservation.reservationId, "visible");
            setTimeout(()=> { location.reload();}, 3000);
            }
    }
       catch(error){
            console.log(error);
    }
}

function getInput(elementId){
    let inputValue = document.getElementById(elementId).value;
    if(inputValue === ""){
       inputValue = document.getElementById(elementId).placeholder;
    }
    return inputValue;
}

function getFilterInput(elementId){
    let inputValue = document.getElementById(elementId).value;
    return inputValue;
}

async function addCancelEvent(){
    document.getElementById("reservation_cancel_button").addEventListener("click", closeMenu);
}

 async function closeMenu(){
       let subMenu = document.getElementById("reservation-form-container");
       subMenu.classList.remove("open_data_menu");
       let tableElement = document.getElementById("table_cont");
       tableElement.style.opacity = 1;
       reservationIdToUpdate="";
}

function addFilterEvent(){
    document.getElementById("find_button").addEventListener("click", filter);
};

async function filter(){
    await getFilterInputData();
    await cleanTable();
    await displayFilteredReservations();
    await addEditReservationEvent();
}

async function getFilterInputData(){
    filterReservationId = getFilterInput("reservation_id");
    console.log("Set filterReservationId: "+ filterReservationId);

    filterRoomNumber = getFilterInput("room_number");
    console.log("Set filterRoomNumber: "+ filterRoomNumber);

    filterFirstName = getFilterInput("first_name");
    console.log("Set filterFirstName: "+ filterFirstName);

    filterLastName = getFilterInput("last_name");
    console.log("Set filterLastName: "+ filterLastName);
}



async function cleanTable(){
    let table = document.getElementById("reservation_table");

    while (table.children.length!==1) {
        table.removeChild(table.lastElementChild);
    }
    console.log(table.children.length);
}

async function displayFilteredReservations(){
    var userReservations;
  let url = "http://localhost:8081/reservations?";
  if (filterReservationId!==""){
    url = url.concat("reservationId="+filterReservationId+"&")
  }
  if (filterFirstName!==""){
     url = url.concat("guestFirstName="+filterFirstName+"&")
  }

  if (filterLastName!==""){
      url = url.concat("guestLastName="+filterLastName+"&")
  }
  if (filterRoomNumber!==""){
       url = url.concat("roomNumber="+filterRoomNumber+"&")
  }

  console.log("Filtered URL: "+ url);
  userReservations = await getData(url, token);

  await displayReservationsTable(userReservations);
}

