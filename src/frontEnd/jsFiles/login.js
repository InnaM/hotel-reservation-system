localStorage.clear();

var btnLogin = document.getElementById("login_button");
btnLogin.addEventListener("click", validateInputCredentials);

var newAccBtn = document.getElementById("new_account_button");
newAccBtn.addEventListener("click", newAccountWindow);

async function validateInputCredentials(){
let log_user = document.getElementById("username").value.toString();
let log_password= document.getElementById("password").value.toString();

var isValid = await validateCredentials(log_user, log_password);
if (isValid == true){
    console.log(log_user +" login successfully");
    window.location = "menu.html";
} else {
    getMessage("errorMsg","Wrong username/password or you don't have account", "visible" );
    setTimeout(()=> { getMessage("errorMsg","", "hidden");}, 3000);
 }
}


function getMessage(id, text, visibility){
         const msg = document.getElementById(id);
         msg.textContent = text;
         msg.style.visibility = visibility;
 }

async function getToken(url, username,password ){
    let bodyData = {username:username, password:password};
    try {
        const response = await fetch(url, {
                                  method: "POST",
                                  body: JSON.stringify(bodyData),
                                  headers: {"Content-Type": "application/json"}
                                  });
        if (!response.ok){
            throw new Error("Could not fetch resource");
        } else {
        const text = await response.text();
        const token = "Bearer ".concat(String(text));
        console.log(token);
        return  token;
        }
    }
    catch(error){
      console.error(error);
      return "";
    }
 }

 async function getUserRole(url, token){
 console.log("///////////////////////////"+token);
      try {

          const response = await fetch(url, {
                                        method: "GET",
                                        headers: {
                                        "Authorization": token, "Accept":"application/json"}
                                        });
          if (!response.ok){
              throw new Error("Could not fetch resource");
          }
          const role = await response.json();
          console.log("Fetch user Role by username: " + role );
          return role;
          }
          catch(error){
            console.error(error)
          }
  }

 async function validateCredentials(log_name, log_password){
    const token = await  getToken("http://localhost:8081/token", log_name, log_password);
    if(token!=="" && token.startsWith("Bearer")){
        localStorage.setItem("username", log_name);
        localStorage.setItem("password", log_password);
        localStorage.setItem("token", token);
        const role = await getUserRole("http://localhost:8081/user/role/"+log_name, token)
        localStorage.setItem("role", role);
        return true;
     } else {
        return false;
     }
 }

function newAccountWindow(){
    window.location = "newAccount.html";
}

