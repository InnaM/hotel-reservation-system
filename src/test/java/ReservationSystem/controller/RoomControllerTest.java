package ReservationSystem.controller;

import ReservationSystem.data.dto.RoomDTO;
import ReservationSystem.data.entity.BedInfo;

import ReservationSystem.service.AvailableRoomRequest;
import ReservationSystem.service.RoomService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(RoomController.class)
@AutoConfigureMockMvc(addFilters = false)
class RoomControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private RoomService roomService;

    private RoomDTO room1;

    private RoomDTO room2;


    @BeforeEach
    @WithMockUser(roles = {"ADMIN"})
    void setUp(){
       room1 = new RoomDTO("101", BedInfo.TWIN_SINGLES);
       room2 = new RoomDTO("102", BedInfo.TWIN_SINGLES);
    }

    @Test
    void should_return_available_rooms() throws Exception {
        //given
        when(roomService.getAvailableRooms(any(AvailableRoomRequest.class))).thenReturn(List.of(room1, room2));
        //when and then
        mvc.perform(post("/rooms/availableRooms")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(
                                "{\n" +
                                        "    \"bedInfo\": \"TWIN_SINGLES\",\n" +
                                        "    \"startDate\": \"01.08.2024 14:00\",\n" +
                                        "     \"endDate\": \"10.08.2024 12:00\"\n" +
                                        "}"
                        ))

                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].roomNumber", equalTo("101")))
                .andExpect(jsonPath("$[1].roomNumber", equalTo("102")))
                .andExpect(jsonPath("$[0].bedInfo", equalTo("TWIN_SINGLES")));
    }

    @Test
    void should_add_not_exist_room() throws Exception {
        //given
        when(roomService.addRoom(room1)).thenReturn(room1);
        //when and then
        mvc.perform(post("/rooms")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(
                                "{\"roomNumber\":\"101\", " +
                                        "\"bedInfo\":\"TWIN_SINGLES\"}"
                        ))

                .andExpect(status().isOk())
                .andExpect(jsonPath("$.roomNumber", equalTo("101")))
                .andExpect(jsonPath("$.bedInfo", equalTo("TWIN_SINGLES")));
    }

    @Test
    void should_delete_by_room_number() throws Exception {
        //given
        when(roomService.deleteByNumber(room1.getRoomNumber())).thenReturn(room1);
        //when and then
        mvc.perform(delete("/rooms/"+room1.getRoomNumber())
                        .contentType(MediaType.APPLICATION_JSON))

                .andExpect(status().isOk())
                .andExpect(jsonPath("$.roomNumber", equalTo("101")))
                .andExpect(jsonPath("$.bedInfo", equalTo("TWIN_SINGLES")));
    }

    @Test
    void should_get_all_rooms() throws Exception {
        //given
        when(roomService.getAll("desc")).thenReturn(List.of(room2, room1));
        //when and then
        mvc.perform(get("/rooms?direction=desc")
                        .contentType(MediaType.APPLICATION_JSON))

                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].roomNumber", equalTo("102")))
                .andExpect(jsonPath("$[1].roomNumber", equalTo("101")))
                .andExpect(jsonPath("$[0].bedInfo", equalTo("TWIN_SINGLES")));
    }





}