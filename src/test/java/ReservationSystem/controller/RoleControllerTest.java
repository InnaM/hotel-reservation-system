package ReservationSystem.controller;

import ReservationSystem.data.entity.EnumRole;
import ReservationSystem.data.entity.Role;
import ReservationSystem.service.RoleService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(RoleController.class)
@AutoConfigureMockMvc(addFilters = false)
public class RoleControllerTest {
    @Autowired
    private MockMvc mvc;
    @MockBean
    private RoleService roleService;

    private Role adminRole;
    private Role userRole;
    private Role testRole;
    Set<String> users;

    @BeforeEach
    @WithMockUser(roles = {"ADMIN"})
    void setUp() {
        users = new HashSet<>();
        adminRole = new Role(1L, EnumRole.ADMIN, users);
        userRole = new Role(2L, EnumRole.USER, users);
        testRole = new Role(4L, EnumRole.TEST, users);
    }

    @Test
    public void should_not_allow_access_to_unauthenticated_users() throws Exception {
        mvc.perform(get("/auth/roles")).andExpect(status().isOk());
    }

    @Test
    void should_return_list_of_roles() throws Exception {
        //given
        when(roleService.allRoles()).thenReturn(List.of(adminRole, userRole));
        //when and then
        mvc.perform(get("/auth/roles")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name", equalTo("ADMIN")))
                .andExpect(jsonPath("$[1].name", equalTo("USER")));
    }

    @Test
    void should_return_role_by_name() throws Exception {
        //given
        when(roleService.getRoleByName("admin")).thenReturn(adminRole);
        //when and then
        mvc.perform(get("/auth/roles/admin")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", equalTo("ADMIN")))
                .andExpect(jsonPath("$.id", equalTo(1)));
    }

    @Test
    void should_delete_role_by_name() throws Exception {
        //given
        when(roleService.deleteRoleByName("test")).thenReturn(testRole);
        //when and then
        mvc.perform(delete("/auth/roles/delete/test")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", equalTo("TEST")))
                .andExpect(status().isOk());
    }

    @Test
    void should_add_not_exist_role() throws Exception {
        //given
        when(roleService.saveRole(testRole)).thenReturn(testRole);
        //when and then
        mvc.perform(post("/auth/roles/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":4,\"name\":\"TEST\",\"users\":[]}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", equalTo(4)))
                .andExpect(jsonPath("$.name", equalTo("TEST")));
    }

}