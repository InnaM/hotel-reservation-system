package ReservationSystem.controller;

import ReservationSystem.data.dto.GuestDTO;
import ReservationSystem.service.GuestService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(GuestController.class)
@AutoConfigureMockMvc(addFilters = false)
public class GuestControllerTest {

    @Autowired
    private MockMvc mvc;

    private GuestDTO guest1;

    private GuestDTO guest2;


    @MockBean
    private GuestService guestService;

    @BeforeEach
    @WithMockUser(roles = {"ADMIN"})
    void setUp() {
        guest1 = new GuestDTO("guest1", "Adam", "Smith", "Guest1", "email1@gmail.com", "Address1", "23-984534623");
        guest2 = new GuestDTO("guest2", "Maria", "Nowak", "Guest2", "email2@gmail.pl", "Address2", "23-984534623");
    }

    @Test
    void should_add_not_exist_guest() throws Exception {
        //given
        when(guestService.addGuestByUser(guest1)).thenReturn(guest1);
        //when and then
        mvc.perform(post("/guests/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(
                                " {\n" +
                                        "        \"login\": \"guest1\",\n" +
                                        "        \"firstName\": \"Adam\",\n" +
                                        "        \"lastName\": \"Smith\",\n" +
                                        "        \"password\": \"Guest1\",\n" +
                                        "        \"email\": \"email1@gmail.com\",\n" +
                                        "        \"address\": \"Address1\",\n" +
                                        "        \"phoneNumber\": \"23-984534623\"\n" +
                                        "    }"
                        ))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.login", equalTo("guest1")))
                .andExpect(jsonPath("$.firstName", equalTo("Adam")))
                .andExpect(jsonPath("$.lastName", equalTo("Smith")));
    }

    @Test
    void should_delete_exist_guest_by_login() throws Exception {
        //given
        when(guestService.deleteByLogin(guest1.getLogin())).thenReturn(guest1);
        //when and then
        mvc.perform(delete("/guests/delete/" + guest1.getLogin())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.login", equalTo("guest1")))
                .andExpect(jsonPath("$.firstName", equalTo("Adam")))
                .andExpect(jsonPath("$.lastName", equalTo("Smith")));
    }

    @Test
    void should_update_guest() throws Exception {
        //given
        when(guestService.updateGuest(guest1)).thenReturn(guest1);
        //when and then
        mvc.perform(put("/guests/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(
                                " {\n" +
                                        "        \"login\": \"guest1\",\n" +
                                        "        \"firstName\": \"Adam\",\n" +
                                        "        \"lastName\": \"Smith\",\n" +
                                        "        \"password\": \"Guest1\",\n" +
                                        "        \"email\": \"email1@gmail.com\",\n" +
                                        "        \"address\": \"Address1\",\n" +
                                        "        \"phoneNumber\": \"23-984534623\"\n" +
                                        "    }"
                        ))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.login", equalTo("guest1")))
                .andExpect(jsonPath("$.firstName", equalTo("Adam")))
                .andExpect(jsonPath("$.lastName", equalTo("Smith")));
    }

    @Test
    void should_get_all_guests() throws Exception {
        //given
        when(guestService.getAll()).thenReturn(List.of(guest1, guest2));
        //when and then
        mvc.perform(get("/guests")
                        .contentType(MediaType.APPLICATION_JSON))

                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].login", equalTo("guest1")))
                .andExpect(jsonPath("$[1].login", equalTo("guest2")))
                .andExpect(jsonPath("$[0].firstName", equalTo("Adam")))
                .andExpect(jsonPath("$[0].lastName", equalTo("Smith")))
                .andExpect(jsonPath("$[1].lastName", equalTo("Nowak")));
    }
}

