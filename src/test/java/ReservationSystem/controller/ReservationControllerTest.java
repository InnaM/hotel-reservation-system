package ReservationSystem.controller;

import ReservationSystem.data.dto.ReservationDTO;
import ReservationSystem.service.ReservationService;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ReservationController.class)
@AutoConfigureMockMvc(addFilters = false)
class ReservationControllerTest {

    @Autowired
    private MockMvc mvc;

    private ReservationDTO reservation1;

    private ReservationDTO reservation2;

    @MockBean
    private ReservationService reservationService;

    @BeforeEach
    @WithMockUser(roles = {"ADMIN"})
    void setUp() {
        reservation1 = new ReservationDTO("111-222-3", "101", "", "Adam",
                "Smith", "email1@gmail.com", "Address1", "23-984534623",
                LocalDateTime.of(2024, 9, 1, 14, 00),
                LocalDateTime.of(2024, 9, 5, 12, 00));

        reservation2 = new ReservationDTO("111-223-1", "102", null, "Maria",
                "Nowak", "email2@gmail.com", "Address2", "45-984534456",
                LocalDateTime.of(2024, 9, 11, 14, 00),
                LocalDateTime.of(2024, 9, 17, 12, 00));


    }

    @Test
    void should_add_reservation_by_admin() throws Exception {
        //given
        when(reservationService.addReservationByAdmin(reservation1)).thenReturn(reservation1);
        //when and then

        String result = mvc.perform(post("/reservations/addByAdmin")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                        " {\n" +
                                "\"roomNumber\": \"101\",\n" +
                                "  \"guestFirstName\": \"Adam\",\n" +
                                "  \"guestLastName\": \"Smith\",\n" +
                                "  \"guestEmail\": \"email1@gmail.com\",\n" +
                                "  \"guestAddress\": \"Address1\",\n" +
                                "  \"guestPhoneNumber\": \"23-984534623\",\n" +
                                "  \"startDate\": \"01.09.2024 14:00\",\n" +
                                "  \"endDate\": \"05.09.2024 12:00\"\n" +
                                " }"
                )).andReturn().getResponse().getContentAsString();
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(!result.isEmpty());
            softly.assertThat(result.contains(reservation1.getRoomNumber()));
            softly.assertThat(result.contains(reservation1.getReservationId()));
            softly.assertThat(result.contains(reservation1.getGuestLastName()));
            softly.assertThat(result.contains("05.09.2024 12:00"));
        });
    }

    @Test
    void should_update_reservation() throws Exception {
        //given
        when(reservationService.updateReservation(reservation1)).thenReturn(reservation1);
        //when and then
        String result = mvc.perform(put("/reservations/update/" + reservation1.getReservationId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                        " {\n" +
                                "        \"reservationId\": \"111-222-3\",\n" +
                                "        \"roomNumber\": \"101\",\n" +
                                "        \"guestFirstName\": \"Adam\",\n" +
                                "        \"guestLastName\": \"Smith\",\n" +
                                "        \"guestEmail\": \"email1@gmail.com\",\n" +
                                "        \"guestAddress\": \"Address1\",\n" +
                                "        \"guestPhoneNumber\": \"23-984534623\",\n" +
                                "        \"startDate\": \"01.09.2024 14:00\",\n" +
                                "        \"endDate\": \"05.09.2024 12:00\"\n" +
                                "    }"
                )).andReturn().getResponse().getContentAsString();
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(!result.isEmpty());
            softly.assertThat(result.contains(reservation1.getRoomNumber()));
            softly.assertThat(result.contains(reservation1.getReservationId()));
            softly.assertThat(result.contains(reservation1.getGuestLastName()));
            softly.assertThat(result.contains("05.09.2024 12:00"));
        });
    }

    @Test
    void should_get_all_reservations() throws Exception {
        //given
        when(reservationService.getAll()).thenReturn(List.of(reservation1, reservation2));
        //when and then
        String result = mvc.perform(get("/reservations")
                        .contentType(MediaType.APPLICATION_JSON))
                  .andReturn().getResponse().getContentAsString();
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(!result.isEmpty());
            softly.assertThat(result.contains(reservation1.getRoomNumber()));
            softly.assertThat(result.contains(reservation2.getRoomNumber()));
            softly.assertThat(result.contains(reservation1.getGuestLastName()));
            softly.assertThat(result.contains("01.09.2024 14:00"));
            softly.assertThat(result.contains("17.09.2024 12:00"));
        });
    }

    @Test
    void should_delete_reservations() throws Exception {
        //given
        when(reservationService.deleteById(reservation1.getReservationId())).thenReturn(reservation1);
        //when and then
        mvc.perform(delete("/reservations/delete/" + reservation1.getReservationId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.roomNumber", equalTo(reservation1.getRoomNumber())))
                .andExpect(jsonPath("$.reservationId", equalTo(reservation1.getReservationId())))
                .andExpect(jsonPath("$.guestFirstName", equalTo(reservation1.getGuestFirstName())))
                .andExpect(jsonPath("$.guestLastName", equalTo("Smith")))
                .andExpect(jsonPath("$.startDate", equalTo("01.09.2024 14:00")));
    }

    @Test
    void should_get_reservation_by_id() throws Exception {
        //given
        when(reservationService.getById(reservation1.getReservationId())).thenReturn(reservation1);
        //when and then
        mvc.perform(get("/reservations/get/" + reservation1.getReservationId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.roomNumber", equalTo(reservation1.getRoomNumber())))
                .andExpect(jsonPath("$.reservationId", equalTo(reservation1.getReservationId())))
                .andExpect(jsonPath("$.guestFirstName", equalTo(reservation1.getGuestFirstName())))
                .andExpect(jsonPath("$.guestLastName", equalTo("Smith")))
                .andExpect(jsonPath("$.startDate", equalTo("01.09.2024 14:00")));
    }
}