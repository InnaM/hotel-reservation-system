package ReservationSystem.controller;

import ReservationSystem.data.dto.EmployeeDTO;
import ReservationSystem.service.EmployeeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(HotelEmployeeController.class)
@AutoConfigureMockMvc(addFilters = false)
class HotelEmployeeControllerTest {

    @Autowired
    private MockMvc mvc;

    private EmployeeDTO employee1;

    private EmployeeDTO employee2;


    @MockBean
    private EmployeeService employeeService;

    @BeforeEach
    @WithMockUser(roles = {"ADMIN"})
    void setUp() {
        employee1 = new EmployeeDTO("jkowalski","Jan Kowalski",
                "JKowalski1", "Manager");
        employee2 = new EmployeeDTO("bkonrad","Barbara Konrad",
                "BKonrad1", "Receptionist");
    }

    @Test
    void should_get_all_employees() throws Exception {
        //given
        when(employeeService.getAllEmployees()).thenReturn(List.of(employee1, employee2));
        //when and then
        mvc.perform(get("/auth/employees")
                        .contentType(MediaType.APPLICATION_JSON))

                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].username", equalTo("jkowalski")))
                .andExpect(jsonPath("$[1].username", equalTo("bkonrad")))
                .andExpect(jsonPath("$[0].fullName", equalTo("Jan Kowalski")))
                .andExpect(jsonPath("$[1].fullName", equalTo("Barbara Konrad")))
                .andExpect(jsonPath("$[1].position", equalTo("Receptionist")));
    }

    @Test
    void should_add_not_exist_employee() throws Exception {
        //given
        when(employeeService.addEmployee(employee1)).thenReturn(employee1);
        //when and then
        mvc.perform(post("/auth/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(
                                "     {\n" +
                                        "        \"username\": \"jkowalski\",\n" +
                                        "        \"fullName\": \"Jan Kowalski\",\n" +
                                        "        \"password\": \"JKowalski1\",\n" +
                                        "        \"position\": \"Manager\"\n" +
                                        "    }"
                        ))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.username", equalTo("jkowalski")))
                .andExpect(jsonPath("$.fullName", equalTo("Jan Kowalski")))
                .andExpect(jsonPath("$.position", equalTo("Manager")));
    }

    @Test
    void should_update_exist_employee() throws Exception {
        //given
        when(employeeService.updateEmployee(employee1)).thenReturn(employee1);
        //when and then
        mvc.perform(put("/auth/employees/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(
                                "     {\n" +
                                        "        \"username\": \"jkowalski\",\n" +
                                        "        \"fullName\": \"Jan Kowalski\",\n" +
                                        "        \"password\": \"JKowalski1\",\n" +
                                        "        \"position\": \"Manager\"\n" +
                                        "    }"
                        ))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.username", equalTo("jkowalski")))
                .andExpect(jsonPath("$.fullName", equalTo("Jan Kowalski")))
                .andExpect(jsonPath("$.position", equalTo("Manager")));
    }

    @Test
    void should_delete_exist_employee() throws Exception {
        //given
        when(employeeService.deleteByUsername(employee1.getUsername())).thenReturn(employee1);
        //when and then
        mvc.perform(delete("/auth/employees/delete/"+employee1.getUsername())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.username", equalTo("jkowalski")))
                .andExpect(jsonPath("$.fullName", equalTo("Jan Kowalski")))
                .andExpect(jsonPath("$.position", equalTo("Manager")));
    }

    @Test
    void should_get_employee_by_username() throws Exception {
        //given
        when(employeeService.getEmployeeByUserName(employee1.getUsername())).thenReturn(employee1);
        //when and then
        mvc.perform(get("/auth/employees/"+employee1.getUsername())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.username", equalTo("jkowalski")))
                .andExpect(jsonPath("$.fullName", equalTo("Jan Kowalski")))
                .andExpect(jsonPath("$.position", equalTo("Manager")));
    }
}