package ReservationSystem.mapper;


import ReservationSystem.data.dto.EmployeeDTO;
import ReservationSystem.data.entity.Employee;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;

class EmployeeMapperTest {
    EmployeeMapper mapper = new ReservationSystem.mapper.EmployeeMapperImpl();

    @Test
    void should_map_employee_to_dto(){
        //given
        Employee manager = new Employee("jk", "Jan Kowalski",
                "jk", "Manager");
        //when
        EmployeeDTO employeeDTO = mapper.toDto(manager);
        //then
        SoftAssertions.assertSoftly(softly->{
            softly.assertThat(employeeDTO.getFullName()).isEqualTo("Jan Kowalski");
            softly.assertThat(employeeDTO.getUsername()).isEqualTo("jk");
            softly.assertThat(employeeDTO.getPassword()).isEqualTo("jk");
            softly.assertThat(employeeDTO.getPosition()).isEqualTo("Manager");
        });


    }

    @Test
    void should_map_dto_to_employee(){
        //given
        EmployeeDTO managerDto = new EmployeeDTO( "jk","Jan Kowalski",
                 "jk", "Manager");
        //when
        Employee employee = mapper.fromDto(managerDto);
        //then
        SoftAssertions.assertSoftly(softly->{
            softly.assertThat(employee.getFullName()).isEqualTo("Jan Kowalski");
            softly.assertThat(employee.getUsername()).isEqualTo("jk");
            softly.assertThat(employee.getPassword()).isEqualTo("jk");
            softly.assertThat(employee.getPosition()).isEqualTo("Manager");
        });


    }


}