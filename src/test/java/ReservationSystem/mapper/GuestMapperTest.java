package ReservationSystem.mapper;

import ReservationSystem.data.dto.GuestDTO;
import ReservationSystem.data.entity.Guest;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;

import java.util.Collections;


class GuestMapperTest {

    GuestMapper mapper = new ReservationSystem.mapper.GuestMapperImpl();

    @Test
    void should_map_guest_to_dto(){
        //given
        Guest guest = new Guest("111", "guest1", "Adam", "Smith",  "guest1",
                "email1@com","Address1" , "phoneNumber1", Collections.emptyList());
        //when
        GuestDTO guestDTO=mapper.toDto(guest);
        //then
        SoftAssertions.assertSoftly(softly->{
            softly.assertThat(guestDTO.getFirstName()).isEqualTo("Adam");
            softly.assertThat(guestDTO.getLastName()).isEqualTo("Smith");
            softly.assertThat(guestDTO.getLogin()).isEqualTo("guest1");
            softly.assertThat(guestDTO.getPassword()).isEqualTo("guest1");
            softly.assertThat(guestDTO.getAddress()).isEqualTo("Address1");
            softly.assertThat(guestDTO.getEmail()).isEqualTo("email1@com");
            softly.assertThat(guestDTO.getPhoneNumber()).isEqualTo("phoneNumber1");
        });
    }
    @Test
    void should_map_dto_to_guest(){
        //given
        GuestDTO guestDto = new GuestDTO("guest1", "Adam", "Smith",
                "guest1", "email1@com","Address1" , "phoneNumber1");
        //when
        Guest guest=mapper.fromDto(guestDto);
        //then
        SoftAssertions.assertSoftly(softly->{
            softly.assertThat(guest.getFirstName()).isEqualTo("Adam");
            softly.assertThat(guest.getLastName()).isEqualTo("Smith");
            softly.assertThat(guest.getLogin()).isEqualTo("guest1");
            softly.assertThat(guest.getPassword()).isEqualTo("guest1");
            softly.assertThat(guest.getAddress()).isEqualTo("Address1");
            softly.assertThat(guest.getEmail()).isEqualTo("email1@com");
            softly.assertThat(guest.getPhoneNumber()).isEqualTo("phoneNumber1");
        });
    }

}