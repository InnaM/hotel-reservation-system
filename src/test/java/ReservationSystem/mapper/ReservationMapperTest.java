package ReservationSystem.mapper;


import ReservationSystem.data.dto.ReservationDTO;
import ReservationSystem.data.entity.BedInfo;
import ReservationSystem.data.entity.Guest;
import ReservationSystem.data.entity.Reservation;
import ReservationSystem.data.entity.Room;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Collections;

class ReservationMapperTest {
    ReservationMapper reservationMapper = new ReservationMapperImpl();
    @Test
    void should_map_reservation_to_dto(){
        //given
        Room room = new Room("1", BedInfo.TWIN_SINGLES, null);
        Guest guest = new Guest("111", "guest1", "Adam", "Smith",
                "guest1", "email1@com","Address1",  "phoneNumber1", Collections.EMPTY_LIST);
        Reservation reservation = new Reservation(
                "134567-fgh", room, guest,
                LocalDateTime.of(2023, 4, 10, 14, 00),
                LocalDateTime.of(2023, 4, 12, 12, 00));
        //when
        ReservationDTO reservationDTO=reservationMapper.toDto(reservation);
        //then
        SoftAssertions.assertSoftly(softly->{
            softly.assertThat(reservationDTO.getReservationId()).isEqualTo("134567-fgh");
            softly.assertThat(reservationDTO.getGuestLogin()).isEqualTo("guest1");
            softly.assertThat(reservationDTO.getRoomNumber()).isEqualTo("1");
            softly.assertThat(reservationDTO.getStartDate()).isEqualTo(LocalDateTime.of(2023, 4, 10, 14, 00));
            softly.assertThat(reservationDTO.getEndDate()).isEqualTo(LocalDateTime.of(2023, 4, 12, 12, 00));
        });
    }
    @Test
    void should_map_dto_to_reservation(){
        //given
        ReservationDTO reservationDto = new ReservationDTO(
                "134567-fgh", "2", "guest1", "Adam",
                "Smith","email1@com","Address1","phoneNumber1",
                LocalDateTime.of(2023, 4, 10, 14, 00),
                LocalDateTime.of(2023, 4, 12, 12, 00));
        //when
        Reservation reservation=reservationMapper.fromDto(reservationDto);
        //then
        SoftAssertions.assertSoftly(softly->{
            softly.assertThat(reservation.getReservationId()).isEqualTo("134567-fgh");
            softly.assertThat(reservation.getGuest().getLogin()).isEqualTo("guest1");
            softly.assertThat(reservation.getRoom().getRoomNumber()).isEqualTo("2");
            softly.assertThat(reservation.getStartDate()).isEqualTo(LocalDateTime.of(2023, 4, 10, 14, 00));
            softly.assertThat(reservation.getEndDate()).isEqualTo(LocalDateTime.of(2023, 4, 12, 12, 00));
        });
    }
}