package ReservationSystem.mapper;

import ReservationSystem.data.dto.RoomDTO;
import ReservationSystem.data.entity.BedInfo;
import ReservationSystem.data.entity.Room;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;

class RoomMapperTest {
    RoomMapper mapper = new ReservationSystem.mapper.RoomMapperImpl();
    @Test
    public void should_map_room_to_dto(){
        //given
        Room room = new Room( "1", BedInfo.TWIN_SINGLES, null);
        //when
        RoomDTO roomDTO = mapper.toDto(room);
        //then
        SoftAssertions.assertSoftly(softly->{
            softly.assertThat(roomDTO.getRoomNumber()).isEqualTo("1");
            softly.assertThat(roomDTO.getBedInfo()).isEqualTo(BedInfo.TWIN_SINGLES);
        });
    }
    @Test
    public void should_map_dto_to_room(){
        //given
        RoomDTO roomDto = new RoomDTO("1", BedInfo.TWIN_SINGLES);
        //when
        Room room = mapper.fromDto(roomDto);
        //then

        SoftAssertions.assertSoftly(softly->{
            softly.assertThat(room.getRoomNumber()).isEqualTo("1");
            softly.assertThat(room.getBedInfo()).isEqualTo(BedInfo.TWIN_SINGLES);
            softly.assertThat(room.getReservations().isEmpty());
        });
    }

}