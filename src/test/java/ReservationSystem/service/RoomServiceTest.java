package ReservationSystem.service;

import ReservationSystem.data.dto.RoomDTO;
import ReservationSystem.data.entity.BedInfo;
import ReservationSystem.data.entity.Reservation;
import ReservationSystem.data.entity.Room;
import ReservationSystem.mapper.RoomMapper;
import ReservationSystem.repository.ReservationRepository;
import ReservationSystem.repository.RoomRepository;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.*;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class RoomServiceTest {

    @TestConfiguration
    static class RoomServiceTestConfiguration {
        @Bean
        public RoomService roomService(RoomRepository roomRepository, ReservationRepository reservationRepository, RoomMapper mapper) {
            return new RoomService( roomRepository, reservationRepository, mapper);
        }
    }

    @MockBean
    private RoomRepository roomRepository;

    @MockBean
    ReservationRepository reservationRepository;

    @MockBean
    private RoomMapper mapper;
    @Autowired
    private RoomService roomService;

    private Room room;
    private RoomDTO roomDTO;
    private List<Reservation> reservations;

    @BeforeEach
    void setUp() {
        reservations = new ArrayList<>();
        room = new Room("1", BedInfo.TWIN_SINGLES, reservations);
        roomDTO = new RoomDTO("1", BedInfo.TWIN_SINGLES);
    }

    @Test
    void should_add_not_exist_room() {
        //given
        when(mapper.fromDto(roomDTO)).thenReturn(room);
        when(roomRepository.findByRoomNumber(room.getRoomNumber())).thenReturn(Optional.empty());
        when(roomRepository.save(room)).thenReturn(room);
        when(mapper.toDto(room)).thenReturn(roomDTO);
        //when
        RoomDTO result = roomService.addRoom(roomDTO);

        //then
        Mockito.verify(roomRepository).save(room);
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(result.getRoomNumber()).isEqualTo(roomDTO.getRoomNumber());
        });
    }

    @Test
    void should_throw_exception_when_add_exist_room() {
        //given
        when(mapper.fromDto(roomDTO)).thenReturn(room);
        when(roomRepository.findByRoomNumber(room.getRoomNumber())).thenReturn(Optional.of(room));
        //when, then
        assertThrows(IllegalArgumentException.class,
                () -> roomService.addRoom(roomDTO),
                "Room with this number already exist");
    }

    @Test
    void should_delete_exist_room_by_number() {
        //given
        when(roomRepository.findByRoomNumber(room.getRoomNumber())).thenReturn(Optional.of(room));
        when(mapper.toDto(room)).thenReturn(roomDTO);
        //when
        RoomDTO result = roomService.deleteByNumber(roomDTO.getRoomNumber());
        //then
        Mockito.verify(roomRepository).deleteByRoomNumber(room.getRoomNumber());
        assertThat(result).isEqualTo(roomDTO);
    }
    @Test
    void should_throw_exception_when_delete_not_exist_room_by_number() {
        //given
        when(roomRepository.findById(1L)).thenReturn(Optional.empty());

        //when, then
        assertThrows(NoSuchElementException.class,
                () -> roomService.deleteByNumber("1"),
                "Room with this number don't exist");
    }
    @Test
    void should_update_exist_room() {
        //given
        Room roomForUpdate = new Room("1", BedInfo.TWIN_SINGLES, reservations);
        when(mapper.fromDto(roomDTO)).thenReturn(room);
        when(roomRepository.findByRoomNumber(roomDTO.getRoomNumber())).thenReturn(Optional.of(roomForUpdate));
        when(roomRepository.save(roomForUpdate)).thenReturn(room);
        when(mapper.toDto(room)).thenReturn(roomDTO);
        //when
        RoomDTO result = roomService.updateRoomInfo(roomDTO);

        //then
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(result.getRoomNumber()).isEqualTo(roomDTO.getRoomNumber());
        });
    }
    @Test
    void should_throw_exception_when_update_not_exist_room() {
        //given
        when(mapper.fromDto(roomDTO)).thenReturn(room);
        when(roomRepository.findByRoomNumber(room.getRoomNumber())).thenReturn(Optional.empty());

        //when, then
        assertThrows(NoSuchElementException.class,
                () -> roomService.updateRoomInfo(roomDTO),
                "There is no room with Number: " + roomDTO.getRoomNumber());
    }

    @Test
    void should_return_exist_room_by_number() {
        //given
        when(roomRepository.findByRoomNumber(room.getRoomNumber())).thenReturn(Optional.of(room));
        when(mapper.toDto(room)).thenReturn(roomDTO);
        //when
        RoomDTO result = roomService.getByRoomNumber(room.getRoomNumber());
        //then
        assertThat(result).isEqualTo(roomDTO);
    }

    @Test
    void should_throw_exception_when_get_not_exist_room_by_number() {
        //given
        when(roomRepository.findByRoomNumber("1")).thenReturn(Optional.empty());

        //when, then
        assertThrows(NoSuchElementException.class,
                () -> roomService.getByRoomNumber("1"),
                "There is no room with number: 1");
    }
    @Test
    void should_return_all_rooms() {
        //given
        ArgumentCaptor<Sort> sortCaptor = ArgumentCaptor.forClass(Sort.class);
        Room room2 = new Room("2", BedInfo.SINGLE, reservations);
        RoomDTO roomDTO2 = new RoomDTO("2", BedInfo.SINGLE);
        when(roomRepository.findAll(Sort.by(Sort.Order.desc("roomNumber")))).thenReturn(List.of(room2, room));
        when(mapper.toDto(room)).thenReturn(roomDTO);
        when(mapper.toDto(room2)).thenReturn(roomDTO2);
        //when
        List<RoomDTO> result = roomService.getAll("desc");
        //then
        Mockito.verify(roomRepository).findAll(sortCaptor.capture());
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(sortCaptor.getValue().getOrderFor("roomNumber")).isEqualTo(Sort.Order.desc("roomNumber"));
            assertThat(result).isEqualTo(List.of(roomDTO2, roomDTO));
        });
    }


}