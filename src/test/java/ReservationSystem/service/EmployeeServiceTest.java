package ReservationSystem.service;

import ReservationSystem.data.dto.EmployeeDTO;
import ReservationSystem.data.entity.Employee;
import ReservationSystem.data.entity.EnumRole;
import ReservationSystem.data.entity.Role;
import ReservationSystem.data.entity.User;
import ReservationSystem.mapper.EmployeeMapper;
import ReservationSystem.repository.EmployeeRepository;
import ReservationSystem.repository.RoleRepository;
import ReservationSystem.repository.UserRepository;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class EmployeeServiceTest {
    @TestConfiguration
    static class EmployeeServiceTestConfiguration {

        @Bean
        public EmployeeService employeeService(EmployeeRepository employeeRepository, RoleRepository roleRepository, EmployeeMapper mapper, UserRepository userRepository, RoleService roleService) {
            return new EmployeeService(employeeRepository, roleRepository, mapper, userRepository, roleService);
        }
    }

    @MockBean
    private EmployeeRepository employeeRepository;

    @MockBean
    private RoleRepository roleRepository;

    @MockBean
    private UserRepository userRepository;
    @MockBean
    private EmployeeMapper mapper;

    @MockBean
    private RoleService roleService;
    @Autowired
    private EmployeeService employeeService;
    private Employee employee;
    private EmployeeDTO employeeDTO;
    private Role role;
    private Set<String> users;
    private User user;

    @BeforeEach
    void setUp() {
        employee = new Employee("jkowalski", "Jan Kowalski",
                "JKowalski1", "Manager");
        employeeDTO = new EmployeeDTO("jkowalski", "Jan Kowalski",
                "JKowalski1", "Manager");
        role = new Role(1L, EnumRole.ADMIN, users);
        user = new User(employee.getUsername(),
                new BCryptPasswordEncoder().encode(employee.getPassword()), EnumRole.ADMIN);
    }

    @Test
    void should_add_not_exist_employee() {
        //given
        when(roleRepository.findByName(role.getName())).thenReturn(Optional.of(role));
        when(mapper.fromDto(employeeDTO)).thenReturn(employee);
        when(employeeRepository.findByUsername(employee.getUsername())).thenReturn(Optional.empty());
        when(userRepository.save(any(User.class))).thenReturn(user);
        when(employeeRepository.save(employee)).thenReturn(employee);
        when(mapper.toDto(employee)).thenReturn(employeeDTO);
        //when
        EmployeeDTO result = employeeService.addEmployee(employeeDTO);
        //then
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(result.getUsername()).isEqualTo(employeeDTO.getUsername());
            softly.assertThat(result.getPassword()).isEqualTo(employeeDTO.getPassword());
            softly.assertThat(result.getUsername()).isEqualTo(user.getUsername());
            softly.assertThat(result.getPassword()).isEqualTo(employeeDTO.getPassword());
        });
    }

    @Test
    void should_return_employee_by_username() {
        //given
        when(employeeRepository.findByUsername(employee.getUsername())).thenReturn(Optional.of(employee));
        when(mapper.toDto(employee)).thenReturn(employeeDTO);
        //when
        EmployeeDTO result = employeeService.getEmployeeByUserName(employee.getUsername());
        //then
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(result.getUsername()).isEqualTo(employeeDTO.getUsername());
            softly.assertThat(result.getPassword()).isEqualTo(employeeDTO.getPassword());
        });
    }

    @Test
    void should_delete_employee_by_full_name() {
        //given
        when(employeeRepository.findByFullName(employee.getFullName())).thenReturn(Optional.of(employee));
        when(mapper.toDto(employee)).thenReturn(employeeDTO);
        //when
        EmployeeDTO result = employeeService.deleteByFullName(employee.getFullName());
        //then
        Mockito.verify(employeeRepository).deleteByFullName(employee.getFullName());
        assertThat(result).isEqualTo(employeeDTO);
    }

    @Test
    void should_throw_exception_when_add_exist_employee() {
        //given
        when(mapper.fromDto(employeeDTO)).thenReturn(employee);
        when(roleRepository.findByName(role.getName()))
                .thenReturn(Optional.of(role));
        when(employeeRepository.findByUsername(employeeDTO.getUsername()))
                .thenReturn(Optional.of(employee));
        //when
        //then
        assertThrows(IllegalArgumentException.class, () ->
                        employeeService.addEmployee(employeeDTO),
                "This Employee already exist: " + employeeDTO.getUsername());
    }

}