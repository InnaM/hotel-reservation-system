package ReservationSystem.service;

import ReservationSystem.data.entity.EnumRole;
import ReservationSystem.data.entity.Role;
import ReservationSystem.repository.RoleRepository;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.*;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class RoleServiceTest {
    private Set<String> users;
    private Role test;

    @TestConfiguration
    static class RoleServiceTestConfiguration {
        @Bean
        public RoleService roleService(RoleRepository roleRepository) {
            return new RoleService(roleRepository);
        }
    }

    @Autowired
    private RoleService roleService;

    @MockBean
    private RoleRepository roleRepository;

    @BeforeEach
    void setUp() {
        users = new HashSet<>();
        test = new Role(1L, EnumRole.TEST, users);
    }

    @Test
    void should_add_not_exist_role() {
        //given
        when(roleRepository.findByName(test.getName())).thenReturn(Optional.empty());
        when(roleRepository.save(test)).thenReturn(test);
        //then
        Role result = roleService.saveRole(test);
        //when
        Mockito.verify(roleRepository).save(test);
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(result.getName()).isEqualTo(test.getName());
            softly.assertThat(result.getId()).isEqualTo(test.getId());
            softly.assertThat(result.getUsers().isEmpty());
        });
    }

    @Test
    void should_throw_exception_when_add_already_exist_role() {
        //given
        when(roleRepository.findByName(test.getName())).thenReturn(Optional.of(test));
        //when
        //then
        assertThrows(IllegalArgumentException.class,
                () -> roleService.saveRole(test),
                "This Role already exist");
    }

    @Test
    void should_return_exist_role_by_name() {
        //given
        when(roleRepository.findByName(test.getName())).thenReturn(Optional.of(test));
        //when
        Role result = roleService.getRoleByName(
                test.getName().name().toLowerCase().substring(3));
        //then
        assertThat(result).isEqualTo(test);
    }

    @Test
    void should_throw_exception_when_try_to_get_not_exist_role() {
        //given
        when(roleRepository.findByName(test.getName())).thenReturn(Optional.empty());
        //when
        //then
        assertThrows(NoSuchElementException.class,
                () -> roleService.getRoleByName(test.getName().name().substring(0, 2)),
                "There is no such role");
    }

    @Test
    void should_return_all_roles() {
        //given
        when(roleRepository.findAll()).thenReturn(List.of(test));
        //when
        List<Role> result = roleService.allRoles();
        //then
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(result.size()).isEqualTo(1);
            softly.assertThat(result.get(0)).isEqualTo(test);
        });
    }

    @Test
    void should_delete_exist_role_by_name() {
        //given
        when(roleRepository.findByName(test.getName())).thenReturn(Optional.of(test));
        //when
        Role result = roleService.deleteRoleByName(
                test.getName().name().toLowerCase().substring(3));
        //then
        Mockito.verify(roleRepository).delete(test);
        assertThat(result).isEqualTo(test);
    }

    @Test
    void should_throw_exception_when_delete_not_exist_role() {
        //given
        when(roleRepository.findByName(test.getName())).thenReturn(Optional.empty());
        //when
        //then
        assertThatThrownBy(() -> roleService.deleteRoleByName(
                test.getName().name().toLowerCase().substring(2)))
                .isInstanceOf(NoSuchElementException.class)
                .withFailMessage("There is no such role");
    }


}