package ReservationSystem.service;

import ReservationSystem.data.dto.GuestDTO;
import ReservationSystem.data.entity.*;
import ReservationSystem.mapper.GuestMapper;
import ReservationSystem.repository.GuestRepository;
import ReservationSystem.repository.RoleRepository;
import ReservationSystem.repository.UserRepository;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.*;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class GuestServiceTest {
    @TestConfiguration
    static class GuestServiceTestConfiguration {
        @Bean
        public GuestService guestService(GuestMapper guestMapper, RoleRepository roleRepository, UserRepository userRepository, GuestRepository guestRepository, RoleService roleService) {
            return new GuestService( guestMapper, roleRepository, userRepository, guestRepository, roleService);
        }
    }

    @MockBean
    private GuestMapper mapper;
    @MockBean
    private RoleRepository roleRepository;
    @MockBean
    private UserRepository userRepository;
    @MockBean
    private GuestRepository guestRepository;
    @MockBean
    private RoleService roleService;
    @Autowired
    private GuestService guestService;
    private List<Reservation> reservations;
    private Set<User> users;
    private Guest guest;
    private GuestDTO guestDTO;
    private Role userRole;
    private User user;

    @BeforeEach
    void setUp() {
        reservations = new ArrayList<>();
        users = new HashSet<>();
        guest = new Guest("111", "guest1", "Adam", "Smith", "Guest1", "email1@com", "Address1", "phoneNumber1", Collections.emptyList());
        guestDTO = new GuestDTO("guest1", "Adam", "Smith", "Guest1", "email1@com", "Address1", "phoneNumber1");

        userRole = new Role(null, EnumRole.USER, Collections.emptySet());
        user = new User(guest.getLogin(), new BCryptPasswordEncoder().encode(guest.getPassword()), EnumRole.USER);
    }

    @Test
    void should_add_not_exist_guest() {
        //given
        when(roleRepository.findByName(userRole.getName())).thenReturn(Optional.of(userRole));
        when(userRepository.findByUsername(guest.getLogin())).thenReturn(Optional.empty());
        when(guestRepository.findByFirstNameAndLastName(guest.getFirstName(), guest.getLastName())).thenReturn(Optional.empty());
        when(mapper.fromDto(guestDTO)).thenReturn(guest);
        when(userRepository.save(any(User.class))).thenReturn(user);
        when(roleService.addUserToRole(userRole, guest.getFirstName() + " " + guest.getLastName())).thenReturn(true);
        when(guestRepository.save(guest)).thenReturn(guest);
        when(mapper.toDto(guest)).thenReturn(guestDTO);
        //when
        GuestDTO result = guestService.addGuestByUser(guestDTO);
        //then
        Mockito.verify(guestRepository).save(guest);
        Mockito.verify(userRepository).save(any(User.class));

        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(result.getLogin()).isEqualTo(guestDTO.getLogin());
            softly.assertThat(result.getPassword()).isEqualTo(guestDTO.getPassword());
            softly.assertThat(result.getLogin()).isEqualTo(user.getUsername());
            softly.assertThat(result.getFirstName()).isEqualTo(guestDTO.getFirstName());
        });
    }

    @Test
    void should_throw_exception_when_add_exist_guest() {
        //given
        when(roleRepository.findByName(EnumRole.USER)).thenReturn(Optional.of(userRole));
        when(mapper.fromDto(guestDTO)).thenReturn(guest);

        when(userRepository.findByUsername(guest.getLogin())).thenReturn(Optional.of(user));
        //when
        //then
        assertThrows(IllegalArgumentException.class, () -> guestService.addGuestByUser(guestDTO), "User with this login already exist, please change username");
    }

    @Test
    void should_delete_exist_guest_by_id() {
        //given
        when(guestRepository.findByLogin(guest.getLogin())).thenReturn(Optional.of(guest));
        when(mapper.toDto(guest)).thenReturn(guestDTO);
        //when
        GuestDTO result = guestService.deleteByLogin(guest.getLogin());
        //then
        Mockito.verify(guestRepository).deleteByLogin(guest.getLogin());
        Mockito.verify(userRepository).deleteById(guest.getLogin());
        assertThat(result).isEqualTo(guestDTO);
    }

    @Test
    void should_throw_exception_when_delete_not_exist_guest() {
        //given
        when(guestRepository.findByLogin(guest.getLogin())).thenReturn(Optional.empty());

        //when
        //then
        assertThrows(NoSuchElementException.class, () -> guestService.deleteByLogin(guest.getLogin()), "Guest with username: " + guest.getLogin() + " doesn't exist.");
    }


    @Test
    void should_update_exist_guest() {
        //given
        Guest guestToUpdate = new Guest("111", "oldLogin", "Adam", "Smith", "oldPassword1", "email1@com", "Address1", "phoneNumber", reservations);
        User userToUpdate = new User("oldLogin", new BCryptPasswordEncoder().encode("oldPassword1"), EnumRole.USER);
        when(mapper.fromDto(guestDTO)).thenReturn(guest);
        when(guestRepository.findByFirstNameAndLastName(guest.getFirstName(), guest.getLastName())).thenReturn(Optional.of(guestToUpdate));

        when(userRepository.findByUsername(guestToUpdate.getLogin())).thenReturn(Optional.of(userToUpdate));
        when(roleRepository.findByName(userRole.getName())).thenReturn(Optional.of(userRole));
        when(guestRepository.save(guestToUpdate)).thenReturn(guest);
        when(mapper.toDto(guest)).thenReturn(guestDTO);
        //when
        GuestDTO result = guestService.updateGuest(guestDTO);
        //then
        Mockito.verify(guestRepository).save(guestToUpdate);

        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(result.getLogin()).isEqualTo(guestDTO.getLogin());
            softly.assertThat(result.getPassword()).isEqualTo(guestDTO.getPassword());
            softly.assertThat(result.getLogin()).isEqualTo(user.getUsername());
            softly.assertThat(result.getPassword()).isEqualTo(guestDTO.getPassword());
        });
    }

    @Test
    void should_return_exist_guest_by_login() {
        //given
        when(guestRepository.findByLogin(guest.getLogin())).thenReturn(Optional.of(guest));
        when(mapper.toDto(guest)).thenReturn(guestDTO);
        //when
        GuestDTO result = guestService.getByLogin(guest.getLogin());
        //then
        assertThat(result).isEqualTo(guestDTO);
    }


}



