package ReservationSystem.service;

import ReservationSystem.data.dto.ReservationDTO;
import ReservationSystem.data.entity.BedInfo;
import ReservationSystem.data.entity.Guest;
import ReservationSystem.data.entity.Reservation;
import ReservationSystem.data.entity.Room;
import ReservationSystem.mapper.GuestMapper;
import ReservationSystem.mapper.ReservationMapper;
import ReservationSystem.mapper.RoomMapper;
import ReservationSystem.repository.GuestRepository;
import ReservationSystem.repository.ReservationRepository;
import ReservationSystem.repository.RoomRepository;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class ReservationServiceTest {

    @TestConfiguration
    static class ReservationServiceTestConfiguration {
        @Bean
        public ReservationService reservationService(ReservationRepository reservationRepository,
                                                     RoomRepository roomRepository,
                                                     GuestRepository guestRepository,
                                                     RoomService roomService,
                                                     ReservationMapper mapper,
                                                     RoomMapper roomMapper) {
            return new ReservationService(reservationRepository, roomRepository,  guestRepository, roomService,  mapper, roomMapper);
        }
    }

    @MockBean
    private ReservationRepository reservationRepository;

    @MockBean
    private RoomRepository roomRepository;

    @MockBean
    private RoomService roomService;

    @MockBean
    private GuestRepository guestRepository;

    @MockBean
    private ReservationMapper mapper;

    @MockBean
    private RoomMapper roomMapper;

    @Autowired
    private ReservationService reservationService;

    private Reservation reservation;
    private Room room;
    private Guest guest;
    private ReservationDTO reservationDTO;
    private List<Reservation> reservations = new ArrayList<>();

    @BeforeEach
    void setUp() {
        guest = new Guest("145",
                "guest1", "Adam", "Smith", "Guest1",
                "email1@com", "Address1", "phoneNumber1", reservations);
        room = new Room("1", BedInfo.TWIN_SINGLES, reservations);
        reservation = new Reservation(
                "123456789", room, guest,
                LocalDateTime.of(2025, 4, 10, 14, 00),
                LocalDateTime.of(2025, 4, 12, 12, 00));
        reservationDTO = new ReservationDTO(
                "123456789", room.getRoomNumber(), guest.getLogin(), guest.getFirstName(), guest.getLastName(),
                guest.getEmail(), guest.getAddress(), guest.getPhoneNumber(),
                LocalDateTime.of(2025, 4, 10, 14, 00),
                LocalDateTime.of(2025, 4, 12, 12, 00));
    }

    @Test
    void should_add_not_exist_reservation() {
        //given
        when(mapper.fromDto(reservationDTO)).thenReturn(reservation);
        when(reservationRepository
                .findByRoom_RoomNumberAndStartDateLessThanAndEndDateGreaterThanEqual(
                        reservation.getRoom().getRoomNumber(), reservation.getEndDate(), reservation.getStartDate()))
                .thenReturn(Optional.empty());

        when(roomRepository.findByRoomNumber(reservation.getRoom().getRoomNumber()))
                .thenReturn(Optional.of(room));
        when(guestRepository.findByLogin(reservation.getGuest().getLogin()))
                .thenReturn(Optional.of(guest));
        when(reservationRepository.save(reservation)).thenReturn(reservation);
        when(mapper.toDto(reservation)).thenReturn(reservationDTO);
        //when
        ReservationDTO result = reservationService.addReservationByAdmin(reservationDTO);
        //then
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(result.getReservationId()).isEqualTo(reservationDTO.getReservationId());
            softly.assertThat(result.getGuestLogin()).isEqualTo(reservationDTO.getGuestLogin());
            softly.assertThat(result.getRoomNumber()).isEqualTo(reservationDTO.getRoomNumber());
            softly.assertThat(result.getStartDate()).isEqualTo(reservationDTO.getStartDate());

        });
    }

    @Test
    void should_update_exist_reservation() {
        //given
        Reservation reservationToUpdate = new Reservation(
                "123456789", room, guest,
                LocalDateTime.of(2025, 5, 14, 14, 00),
                LocalDateTime.of(2025, 5, 16, 12, 00));
        when(mapper.fromDto(reservationDTO)).thenReturn(reservation);
        when(reservationRepository
                .findById(reservation.getReservationId()))
                .thenReturn(Optional.of(reservationToUpdate));
        when(roomRepository.findByRoomNumber(reservation.getRoom().getRoomNumber()))
                .thenReturn(Optional.of(room));
        when(guestRepository.findByFirstNameAndLastName(reservation.getGuest().getFirstName(), reservation.getGuest().getLastName()))
                .thenReturn(Optional.of(guest));

        when(reservationRepository
                .findByRoom_RoomNumberAndStartDateLessThanAndEndDateGreaterThanEqual(
                        reservation.getRoom().getRoomNumber(), reservation.getEndDate(), reservation.getStartDate()))
                .thenReturn(Optional.empty());

        when(reservationRepository.save(reservationToUpdate)).thenReturn(reservationToUpdate);
        when(mapper.toDto(reservationToUpdate)).thenReturn(reservationDTO);
        //when
        ReservationDTO result = reservationService.updateReservation(reservationDTO);
        //then
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(result.getReservationId()).isEqualTo(reservationDTO.getReservationId());
            softly.assertThat(result.getGuestLogin()).isEqualTo(reservationDTO.getGuestLogin());
            softly.assertThat(result.getRoomNumber()).isEqualTo(reservationDTO.getRoomNumber());
            softly.assertThat(result.getStartDate()).isEqualTo(reservationDTO.getStartDate());
            softly.assertThat(result.getEndDate()).isNotEqualTo(LocalDateTime.of(2025, 5, 16, 12, 00));
        });
    }
    @Test
    void should_delete_exist_reservation_by_id() {
        //given

        when(reservationRepository
                .findById(reservationDTO.getReservationId())).thenReturn(Optional.of(reservation));
        when(mapper.toDto(reservation)).thenReturn(reservationDTO);
        //when
        ReservationDTO result = reservationService.deleteById(reservationDTO.getReservationId());
        //then
        Mockito.verify(reservationRepository).deleteById(reservationDTO.getReservationId());
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(result.getReservationId()).isEqualTo(reservationDTO.getReservationId());
            softly.assertThat(result.getGuestLogin()).isEqualTo(reservationDTO.getGuestLogin());
            softly.assertThat(result.getRoomNumber()).isEqualTo(reservationDTO.getRoomNumber());
            softly.assertThat(result.getStartDate()).isEqualTo(reservationDTO.getStartDate());
            softly.assertThat(result.getEndDate()).isNotEqualTo(LocalDateTime.of(2024, 5, 16, 11, 00));
        });
    }
    @Test
    void should_throw_exception_when_delete_not_exist_reservation() {
        //given
        when(reservationRepository.findById(reservation.getReservationId())).thenReturn(Optional.empty());

        //when
        //then
        assertThrows(NoSuchElementException.class, () ->
                        reservationService.deleteById(reservation.getReservationId()),
                "This reservation doesn't exist");
    }
}